package com.ubn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MgmCustomerApplication {

 public static void main(String[] args) {
	SpringApplication.run(MgmCustomerApplication.class, args);
 }
}
