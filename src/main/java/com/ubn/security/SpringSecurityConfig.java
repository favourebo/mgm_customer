package com.ubn.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
   
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
          http.csrf().disable().
               authorizeRequests()
				.antMatchers("/","/loginstatus","/verifyCustomer/**","/echo","/generateReferralReport","/register-customer","/validate-account-number","/user-auth","/validate-referral-code","/get-analytics-info","/get-analytics-info","/ext")
				.permitAll()
				.antMatchers("/bootstrap/**","/css/**","/js/**","/images/**","/font-awesome/**", "/fonts/**","/webfonts/**", "/alert/**")
				.permitAll()
				.anyRequest().authenticated()	
				.and().formLogin()
             .loginProcessingUrl("/login")
             .loginPage("/user-auth")
             .defaultSuccessUrl("/home")
             .failureHandler(customAuthenticationFailureHandler())
           //  .failureUrl("/login?error=true")
             .usernameParameter("username")
             .passwordParameter("password")
             .permitAll()
             .and().logout().logoutUrl("/logout").logoutSuccessUrl("/loginstatus?logout=true").permitAll();
             //.and().sessionManagement().maximumSessions(1).expiredUrl("/login?expired=true").maxSessionsPreventsLogin(true);
  }

	@Bean
    public AuthenticationFailureHandler customAuthenticationFailureHandler() {
		// TODO Auto-generated method stub
	   return new CustomAuthenticationFailureHandler();
	}



	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception { 
        // Setting Service to find User in the database.
        // And Setting PassswordEncoder
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());     
     }
	
}