package com.ubn.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.ubn.model.UserInfo;

@Controller
public class LoginController {

	@GetMapping("/user-auth")
	public String getLoginPage() {
		return "login";
	}

	@GetMapping("/loginstatus")
	 public String login(Model model, String error, String logout,HttpServletRequest request) {
		 String redirectUrl = "login";
	       if (error != null) {
	    	  redirectUrl = "login";
	    	  model.addAttribute("message","Invalid Credentials"); 
	    	 }
	      if (logout != null) {
	    	  
	           }
	       return redirectUrl;
	}
	  
	 
	 @GetMapping("/home")
	public String getHomePage(HttpSession session){
		  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		  UserInfo userDetails = (UserInfo) authentication.getPrincipal();
		  if (!(authentication instanceof AnonymousAuthenticationToken)) {
                session.setAttribute("fullName", userDetails.getFullName());
				 session.setAttribute("username", userDetails.getUsername());
				 session.setAttribute("phone", userDetails.getMobileNumber());
				 session.setAttribute("email", userDetails.getEmail());
				 session.setAttribute("empNumber", userDetails.getEmpNumber());
				 session.setAttribute("accountType", userDetails.getSegmentDesc());
				 session.setAttribute("channelCode", userDetails.getChannelCode());
		 }
		  return "home";
	   }

}
