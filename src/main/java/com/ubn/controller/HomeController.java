package com.ubn.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	@GetMapping("/")
	public String getHomePage() {
		return "index";
	}
	
	@GetMapping("/ex")
	public String getExt() {
		return "external-redirect-uri";
	}
	
	@GetMapping("/ext")
	public String getExternalLoginPage(Model model,@RequestParam("GXyDgfd62") String username,@RequestParam("HuGseHyt") String password) {
		String returnPage = "page not found";
		if(username != null && password != null) {
				model.addAttribute("xUser",username);
				model.addAttribute("xPass",password);
				model.addAttribute("message","Kindly note that you are being redirected to the MGM portal,\n Do you wish to proceed?");
				returnPage = "external-redirect-uri";
		}
	  return returnPage;
	}

	@GetMapping("/verifyCustomer")
	public String verifyCustomer(Model model,@RequestParam("GXyDgfd62") String accountnumber,@RequestParam("HuGseHyt") String refcode) {
		String returnPage = "page not found";
		if(accountnumber != null && refcode != null) {
			model.addAttribute("xUser",accountnumber);
			model.addAttribute("xPass",refcode);
			model.addAttribute("message","Your account verification was successful,\n Do you wish to proceed?");
		 returnPage = "external-redirect-uri";
	 }
  return returnPage;
 }
}
