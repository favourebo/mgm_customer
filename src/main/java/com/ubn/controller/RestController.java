package com.ubn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ubn.host.RestService;
import com.ubn.model.Request;
import com.ubn.model.Response;
import com.ubn.service.AnalyticsInformationServiceImpl;
import com.ubn.service.ProductRedemptionServiceImpl;
import com.ubn.service.RefCodeValidationServiceImpl;
import com.ubn.service.ReferCustomerServiceImpl;
import com.ubn.service.RegisterCustomerServiceImpl;
import com.ubn.service.ReportManagementServiceImpl;

@Controller
public class RestController {

	@Autowired
	RegisterCustomerServiceImpl registerCustomerService;

	@Autowired
	ReferCustomerServiceImpl referCustomerService;

	@Autowired
	RefCodeValidationServiceImpl refCodeValidationService;

	@Autowired
	AnalyticsInformationServiceImpl analyticsInformationService;

	@Autowired
	ProductRedemptionServiceImpl productRedemptionService;

	@Autowired
	ReportManagementServiceImpl reportManagementService;

	@Autowired
	RestService restService;

	// This validates customer account number and generates an OTP
	@PostMapping("/validate-accountnumber-and-generate-otp")
	public @ResponseBody Response validateAccountNumberAndGenerateOtp(@RequestBody Request request) {
		return registerCustomerService.validateAccountNumberAndGenerateOtpCode(request.getAccountNumber());
	}

	// This validates customer account number and generates an OTP
	@PostMapping("/register-customer")
	public @ResponseBody Response registerCustomer(@RequestBody Request request) {
		return registerCustomerService.validateOtpAndRegisterCustomer(request);
	}

	// helps to ping server
	@GetMapping("/echo")
	public @ResponseBody String echo() {
		return "success";
	}

	@PostMapping("/validate-account-number")
	public @ResponseBody Response validateAccountNumber(@RequestBody Request request) {
		return registerCustomerService.validateAccountNumber(request);
	}

	// This does referral code validation
	@PostMapping("/validate-referral-code")
	public @ResponseBody Response validateReferralCode(@RequestBody Request request) {
		return refCodeValidationService.validateRefCode(request.getRefCode());
	}

	// This does customer referral
	@PostMapping("/refer-customer")
	public @ResponseBody Response referCustomer(@RequestBody Request request) {
		return referCustomerService.referCustomer(request);
	}

	// This methods returns all location
		@GetMapping("/get-all-location")
		public @ResponseBody Response getAllLocation() {
			return referCustomerService.getAllLocation();
		}
		
	// This method returns referral list based on refcode
	@PostMapping("/getReferralByRefCodeAndStatus")
	public @ResponseBody Response getReferralByRefCodeAndStatus(@RequestBody Request request) {
		return referCustomerService.getReferralByRefCodeAndStatus(request);
	}

	// This gets referral code by department and status
	@PostMapping("/get-analytics-info")
	public @ResponseBody Response returnAnalyticsInformation(@RequestBody Request request) {
		return analyticsInformationService.getDashboardCount(request);
	}

	// This returns a list of all referred customer
	@GetMapping("/get-all-refered-customer")
	public @ResponseBody Response getAllReferedCustomer() {
		return referCustomerService.getAllReferedCustomer();
	}

	// This methods returns all product's category
	@GetMapping("/get-all-category")
	public @ResponseBody Response getAllProductCategory() {
		return productRedemptionService.getAllCategory();
	}

	// This methods returns all product's category
	@PostMapping("/get-all-product")
	public @ResponseBody Response getAllProduct(@RequestBody Request request) {
		return productRedemptionService.getAllProduct(request);
	}

	@PostMapping("/redeemPoint")
	public @ResponseBody Response redeemPoint(@RequestBody Request request) {
		return productRedemptionService.redeemPoint(request);
	}

}
