package com.ubn.Utility;



@SuppressWarnings("serial")
public class AuthenticationsException extends Exception {
    
	public AuthenticationsException () {

    }

    public AuthenticationsException (String message) {
        super (message);
    }

    public AuthenticationsException (Throwable cause) {
        super (cause);
    }

    public AuthenticationsException (String message, Throwable cause) {
        super (message, cause);
    }
}