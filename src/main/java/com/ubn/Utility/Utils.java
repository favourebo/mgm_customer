package com.ubn.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;


public class Utils {

   public static String getPendingDatableTarget(String dept, String status) {
	   String result = "";
	   if(status.equals("pending")) {
		   System.out.println(dept);
	   switch(dept.toUpperCase()) {
	   	
	   case "CC":
		   result = "#xContactCenterReferrredCustomerModal";
		   break;
	   case "GH":
		   result = "#xGhPendingReferrredCustomerModal";
		   break;
	   case "RM":
		   result = "#xRmPendingReferrredCustomerModal";
		   break;
	    default:
	    	result = "#xAllHssaAndAdminModal";
		    break;
	   }
     }else if(status.equals("approved")) {
    	  if(dept.equalsIgnoreCase("ADMIN") || dept.equalsIgnoreCase("HSSA")) {
    		 result = "#xAllHssaAndAdminModal";
    	 }else {
    		result = "#xApprovedCustomer";
    	 }
    	 
     }
     else if(status.equals("declined")) {
   	  if(dept.equalsIgnoreCase("ADMIN") || dept.equalsIgnoreCase("HSSA")) {
   		 result = "#xAllHssaAndAdminModal";
   	 }else {
   		result = "#xDeclinedCustomer";
   	 }
   	 
    }
	   return result;
   }
   
  
   
   public static String getStatusColor(String status) {
	   String result = "";
	   switch(status.toUpperCase()) {
	   case "N":
		   result = "green";
		   break;
	   case "D":
		   result = "red";
		   break;
	   case "O":
		   result = "orange";
		   break;
	   }
	   return result;
   }
   public static String nullFormatter(Object valueToBeFormatted) {
		String result = "";
		try {
			if (valueToBeFormatted == null) {
			} else if (valueToBeFormatted instanceof Integer) {
				result = String.valueOf((int) valueToBeFormatted);
			} else if (valueToBeFormatted instanceof String) {
				result = (String) valueToBeFormatted;
			} else {
				result = valueToBeFormatted.toString();
			}
		} catch (Exception e) {
			System.out.println("======System could not convert" + valueToBeFormatted);
		}
		return result;
	}

	public static String getClientIp(HttpServletRequest request) {
		String remoteAddr = "";
		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}
		return remoteAddr;
	}

	public static String encryptString(String data) {
		Cryptography cryptography = new Cryptography("ubnmgm00");
		return cryptography.encryptStringEncoded(data);
	 }

	public static String decryptString(String data) {
		Cryptography cryptography = new Cryptography("ubnmgm00");
		return cryptography.decryptStringEncoded(data);
	}


	
public static boolean checkCredentialsValidity(String date){
		boolean rsp = false;
		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		try {
			Date xDate = sdf.parse(date);
			String currDate = sdf.format(new Date());
		    Date xcurrDate = sdf.parse(currDate);
		    
		    
		    long diff = xcurrDate.getTime()-xDate.getTime();
		    long diffMinutes = diff / (60 * 1000) % 60;
		if(diffMinutes > 1) {
			//return true
			rsp = true;
		 }
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rsp;
	}
}