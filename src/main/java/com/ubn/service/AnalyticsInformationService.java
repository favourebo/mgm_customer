package com.ubn.service;

import com.ubn.model.Request;
import com.ubn.model.Response;

public interface AnalyticsInformationService {
	
	public Response getDashboardCount(Request request);

}
