package com.ubn.service;

import com.ubn.model.Request;
import com.ubn.model.Response;

public interface ReportManagementService {

	public Response generateReferralReport(Request request);

}
