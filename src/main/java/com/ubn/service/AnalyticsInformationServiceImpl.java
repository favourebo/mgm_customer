package com.ubn.service;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubn.Utility.Utils;
import com.ubn.host.RestService;
import com.ubn.model.Request;
import com.ubn.model.Response;

@Service
public class AnalyticsInformationServiceImpl implements AnalyticsInformationService {
	
	@Autowired
	RestService restService;

	@Override
	public Response getDashboardCount(Request request) {
		JSONObject jsonrequest = new JSONObject();
		Response response = null;
			jsonrequest.put("refcode", Utils.decryptString(request.getRefCode()));
			jsonrequest.put("statuscode", request.getStatusCode());
		try {
		     response = restService.getCustomerCount(jsonrequest);
		    if(response != null) {
				JSONArray rspArray =  new JSONArray(response.getData().toString());
				 Map<String,String> statusRsp = new HashMap<String,String>();
				 Float rspFloat = null;
				 int loopSize = rspArray.length();
					//get total customer in db
						 int xtotalcount = 0;
						 String xrowCount = null;
						
						 for(int i = 0; i<loopSize; i++ ) {
							JSONObject jObject = (JSONObject) rspArray.get(i);
							xrowCount = Utils.nullFormatter(jObject.get("countrows"));
							if(xrowCount != null) {
							     xtotalcount+=Integer.valueOf(xrowCount);
							}
						}
				
			if(!request.getCountType().equalsIgnoreCase("percentage")) {
					 for(int i = 0; i<loopSize; i++ ) {
							JSONObject jObject = (JSONObject) rspArray.get(i);
				 switch(Utils.nullFormatter(jObject.get("reqstatus"))) {
						case "C":
							statusRsp.put("approved", Utils.nullFormatter(jObject.get("countrows")));
							break;
						case "O":
							statusRsp.put("pending", Utils.nullFormatter(jObject.get("countrows")));
							break;
						case "D":
							statusRsp.put("declined", Utils.nullFormatter(jObject.get("countrows")));
							break;
						default:
						}
					}	
				}
			else {
			    for(int i = 0; i<loopSize; i++ ) {
					JSONObject jObject = (JSONObject) rspArray.get(i);
				 switch(Utils.nullFormatter(jObject.get("reqstatus"))) {
				 case "C":
					rspFloat = Float.valueOf(Utils.nullFormatter(jObject.get("countrows")))/Float.valueOf(xtotalcount);
					rspFloat = rspFloat*100;
					statusRsp.put("approved", String.valueOf(Math.round(rspFloat)));
					break;
				 case "O":
					rspFloat = Float.valueOf(Utils.nullFormatter(jObject.get("countrows")))/Float.valueOf(xtotalcount);
					rspFloat = rspFloat*100;
					statusRsp.put("pending", String.valueOf(Math.round(rspFloat)));
					break;
				case "D":
					rspFloat = Float.valueOf(Utils.nullFormatter(jObject.get("countrows")))/Float.valueOf(xtotalcount);
					rspFloat = rspFloat*100;
					statusRsp.put("declined", String.valueOf(Math.round(rspFloat)));
					break;
				default:
				}
			}
			}
			response.setData(statusRsp);
		 }
		}catch(Exception e)	{
			//-------An error occured
		}
		return response;
	}
}
