package com.ubn.service;

import com.ubn.model.Request;
import com.ubn.model.Response;

public interface ProductRedemptionService {

	public Response createCategory(Request request);

	public Response createProduct(Request request);

	public Response getAllCategory();

	public Response getAllProduct(Request request);

	public Response getCategoryById(Request request);

	public Response updateDeleteCategory(Request request);

	public Response getProductById(Request request);

	public Response updateDeleteProduct(Request request);

	public Response redeemPoint(Request request);

}
