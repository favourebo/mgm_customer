package com.ubn.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubn.Utility.Utils;
import com.ubn.host.RestService;
import com.ubn.model.Request;
import com.ubn.model.Response;

@Service
public class ReferCustomerServiceImpl implements ReferCustomerService {
	@Autowired
	private RestService restService;

    @Override
	public Response referCustomer(Request request) {
		JSONObject jsonrequest = new JSONObject();
		String[] addressPart  = request.getAddress().split("\\|");
		jsonrequest.put("firstname", request.getFirstName());
		jsonrequest.put("lastname", request.getSurname());
		jsonrequest.put("phone", request.getMobilePhone());
		jsonrequest.put("email", request.getEmail());
		jsonrequest.put("refcode", Utils.decryptString(request.getReferralCode()));
		jsonrequest.put("location",addressPart[1]);	
		jsonrequest.put("locationcode", addressPart[0]);	
		Response response = restService.referCustomer(jsonrequest);
		return response;
	}

    @Override
	public Response getReferalByDeptCodeAndTxnid(Request request) {
		JSONObject jsonrequest = new JSONObject();
	if(request.getStaffId() == null || request.getStaffId() == ""){
		jsonrequest.put("querycode", request.getQueryCode());
		jsonrequest.put("statuscode", request.getStatusCode());
		jsonrequest.put("staffrolecode", "");
	}else{
	    jsonrequest.put("querycode", request.getQueryCode());
		jsonrequest.put("statuscode", request.getStatusCode());
		jsonrequest.put("staffrolecode", request.getStaffId());
	}
		Response response = restService.getReferalByDeptCodeAndTxnid(jsonrequest);
		return response;
	}

    @Override
	public Response getAllReferedCustomer() {
		Response response = restService.getAllReferedCustomer();
		return response;
	}

	@Override
	public Response getCustomerDetailsByTxnid(Request request) {
		JSONObject jsonrequest = new JSONObject();
		jsonrequest.put("transid", request.getTransId());
		jsonrequest.put("querycode", request.getQueryCode());
		Response response = restService.getCustomerDetailsByTxnid(jsonrequest);
		return response;
	}

	@Override
	public Response saveReferalConsent(Request request) {
		JSONObject jsonrequest = new JSONObject();
		System.out.println("gh:"+request.getGroupHead());
		jsonrequest.put("transid", request.getTransId());
		jsonrequest.put("statuscode", request.getStatus());
		jsonrequest.put("venueofcall", request.getVenueOfCall());
		jsonrequest.put("cityofcall", request.getCityOfCall());
		jsonrequest.put("staffrolecode", request.getStaffRoleCode());
		jsonrequest.put("ghcode", request.getGroupHead());
		jsonrequest.put("dateofcall", request.getDateOfCall());
		jsonrequest.put("timeofcall", request.getTimeOfCall());
		Response response = restService.saveReferalConsent(jsonrequest);
		return response;
	 }

	@Override
	public Response saveGroupHeadRemark(Request request) {
		JSONObject jsonrequest = new JSONObject();
		System.out.println("rm:"+request.getRelationshipManager());
	    jsonrequest.put("transid", request.getTransId());
		jsonrequest.put("rmcode", request.getRelationshipManager());
		jsonrequest.put("remark", request.getRemark());
		Response response = restService.saveGroupHeadRemark(jsonrequest);
		return response;
	}
	

	@Override
	public Response saveRMRemark(Request request) {
		JSONObject jsonrequest = new JSONObject();
	    jsonrequest.put("transid", request.getTransId());
	    jsonrequest.put("refcode", request.getRefCode());
	    jsonrequest.put("accountNo", request.getAccountNumber());
		jsonrequest.put("statuscode", request.getStatusCode());
		jsonrequest.put("remark", request.getRemark());
		jsonrequest.put("staffrolecode", "5125682");
		Response response = restService.saveRMRemark(jsonrequest);
		return response;
	}
	
	@Override
	public Response getAllLocation() {
		return restService.getAllLocation();
	}

	@Override
	public Response getReferralByRefCodeAndStatus(Request request) {
		JSONObject jsonrequest = new JSONObject();
	    jsonrequest.put("refcode", Utils.decryptString(request.getRefCode()));
	    jsonrequest.put("statuscode", request.getStatusCode());
		return restService.getReferralByRefCodeAndStatus(jsonrequest);
		
		
	}


}
