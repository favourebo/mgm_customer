package com.ubn.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubn.host.RestService;
import com.ubn.model.Request;
import com.ubn.model.Response;

@Service
public class ReportManagementServiceImpl implements ReportManagementService{
	@Autowired
	private RestService restService;
	
	  SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd-yyyy");
	  SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public Response generateReferralReport(Request request) {
		//initializing object
		JSONObject jsonRequest = new JSONObject();
		String xStartDate = null;
		String xEndDate = null;
		try {
			Date newStartDate = sdf2.parse(request.getStartDate());
			Date newEndDate = sdf2.parse(request.getEndDate());
			
			 xStartDate = sdf1.format(newStartDate).replace("-", "/");
			 xEndDate = sdf1.format(newEndDate).replace("-", "/");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//initializing url
		String url = null;
		switch(request.getReportType()) {
		case "REFERRAL DATE":
			jsonRequest.put("startdate", xStartDate);
			jsonRequest.put("enddate", xEndDate);
			url = "getReferedCustomerByDateRange";
			break;
		case "REFERRED CUSTOMER":
			jsonRequest.put("startdate", xStartDate);
			jsonRequest.put("enddate", xEndDate);
			jsonRequest.put("statuscode", request.getStatusCode());
			url = "getReferedCustomerByStatus";
			break;
		
		case "REFERRAL STATUS":
			break;
		
		case "ACCUMULATED POINTS BY USER":
			jsonRequest.put("minpoint", request.getMinPoint());
			jsonRequest.put("maxpoint", request.getMaxPoint());
			url = "getCustomerByPointRange";
		    break;
		
		case "USER LOCATION":
			jsonRequest.put("location", request.getAddress());
			url = "getReferedCustomerByLocation";
            break;
		}
		return restService.generateReferralReport(url,jsonRequest);
	}

}
