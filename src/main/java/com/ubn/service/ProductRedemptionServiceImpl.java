package com.ubn.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ubn.host.RestService;
import com.ubn.model.RedemptionList;
import com.ubn.model.Request;
import com.ubn.model.Response;

@Service
public class ProductRedemptionServiceImpl implements ProductRedemptionService {
	
	@Autowired
	RestService restService;

	
@Override
public Response createCategory(Request request){
	JSONObject jsonRequest = new JSONObject();
	jsonRequest.put("catname", request.getCategoryName());
	jsonRequest.put("catdesc", request.getCategoryDescription());
	return restService.createCategory(jsonRequest);
}

@Override
public Response createProduct(Request request) {
	JSONObject jsonRequest = new JSONObject();
	jsonRequest.put("catid", request.getCategoryId());
	jsonRequest.put("productname", request.getProductName());
	jsonRequest.put("pointperproduct", request.getPointPerProduct());
	jsonRequest.put("isAvail", request.getAvailabilityStatus());
	return restService.createProduct(jsonRequest);
}

@Override
public Response getAllCategory() {
	 return restService.getAllCategory();
}

@Override
public Response getAllProduct(Request request) {
	JSONObject jsonRequest = new JSONObject();
	jsonRequest.put("statuscode", request.getAvailabilityStatus());
	return restService.getAllProduct(jsonRequest);
}

@Override
public Response getCategoryById(Request request) {
	JSONObject jsonRequest = new JSONObject();
	jsonRequest.put("catid", request.getCategoryId());
	return restService.getCategoryById(jsonRequest);
}

@Override
public Response getProductById(Request request) {
	JSONObject jsonRequest = new JSONObject();
	jsonRequest.put("productid", request.getProductId());
	return restService.getProductById(jsonRequest);
}

@Override
public Response updateDeleteCategory(Request request) {
    JSONObject jsonRequest = new JSONObject();
	jsonRequest.put("querycode", request.getQueryCode());
	jsonRequest.put("catid", request.getCategoryId());
	jsonRequest.put("catname", request.getCategoryName());
	jsonRequest.put("catdesc", request.getCategoryDescription());
	return restService.updateDeleteCategory(jsonRequest);
}
 

@Override
public Response updateDeleteProduct(Request request) {
	JSONObject jsonRequest = new JSONObject();
	jsonRequest.put("querycode", request.getQueryCode());
	jsonRequest.put("productid", request.getProductId());
	jsonRequest.put("productname", request.getProductName());
	jsonRequest.put("pointperproduct", request.getPointPerProduct());
	jsonRequest.put("catid", request.getCategoryId());
	jsonRequest.put("isAvail", request.getAvailabilityStatus());
	return restService.updateDeleteProduct(jsonRequest);
}

@Override
public Response redeemPoint(Request request) {
	JSONObject jsonRequest = new JSONObject();
	//JSONArray jsonRedemptionList = new JSONArray();
	   int sum = 0;
		 for(RedemptionList rList: request.getRedemptionList()) {
			sum += Integer.valueOf(rList.getTotalpoint());
		 }
	jsonRequest.put("staffrolecode", request.getStaffRoleCode());
	jsonRequest.put("refcode", request.getRefCode());
	jsonRequest.put("redemptionlist", request.getRedemptionList());
	jsonRequest.put("maxpoint", sum);
	return restService.redeemPoint(jsonRequest);
}

}
