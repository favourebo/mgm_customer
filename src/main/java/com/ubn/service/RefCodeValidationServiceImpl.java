package com.ubn.service;


import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubn.Utility.Utils;
import com.ubn.host.RestService;
import com.ubn.model.Response;

@Service
public class RefCodeValidationServiceImpl implements RefCodeValidationService{
     @Autowired
     public RestService restService;
     
	
	
	@Override
	public Response validateRefCode(String refCode) {
		JSONObject request = new JSONObject();
		request.put("refcode", Utils.decryptString(refCode));
		
		Response response = restService.validateReferralCode(request);
		return response;
	}
	
	}
