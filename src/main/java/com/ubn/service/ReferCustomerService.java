package com.ubn.service;

import com.ubn.model.Request;
import com.ubn.model.Response;

public interface ReferCustomerService {
   public Response referCustomer(Request request);
   public Response getReferalByDeptCodeAndTxnid(Request request);
   public Response getAllReferedCustomer();
   public Response getCustomerDetailsByTxnid(Request request);
   public Response saveReferalConsent(Request request);
   public Response saveGroupHeadRemark(Request request);
   public Response saveRMRemark(Request request);
   public Response getAllLocation();
   public Response getReferralByRefCodeAndStatus(Request request);
}
