package com.ubn.service;

import com.ubn.model.Request;
import com.ubn.model.Response;

public interface RegisterCustomerService {
   public Response validateAccountNumberAndGenerateOtpCode(String accountnumber);
   public Response validateOtpAndRegisterCustomer(Request req);
   public Response validateAccountNumber(Request request);
}
