package com.ubn.service;

import com.ubn.model.Response;

public interface RefCodeValidationService {
  public Response validateRefCode(String refCode);

}
