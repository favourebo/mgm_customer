package com.ubn.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ubn.host.RestService;
import com.ubn.model.Request;
import com.ubn.model.Response;

@Service
public class RegisterCustomerServiceImpl implements RegisterCustomerService{

	@Autowired
	private RestService restService;

	@Override
	public Response validateAccountNumberAndGenerateOtpCode(String accountnumber) {
		JSONObject request = new JSONObject();
		request.put("accountNo", accountnumber);
    	Response response = restService.validateCustomerAndGenerateOtp(request);
		return response;
	}

	@Override
	public Response validateOtpAndRegisterCustomer(Request req) {
		JSONObject request = new JSONObject();
		request.put("accountNo", req.getAccountNumber());
		request.put("isadmin", "C");
		request.put("channelCode", "MGM_WEB");
        Response response = restService.validateOtpAndRegisterCustomer(request);
		return response;
	}

	@Override
	public Response validateAccountNumber(Request request) {
		Response response = new Response("99", "An unknown error occurred!", null);
		// -----validate captcha
		response = restService.validateCaptcha(request.getCaptcha());
		if (!response.getResponseCode().equals("00")) {
			response = new Response("02", "Invalid captcha supplied!", null);
		} else {
			JSONObject jsonrequest = new JSONObject();
			jsonrequest.put("accountNo", request.getAccountNumber());
			response = restService.validateAccountNumber(jsonrequest);
		}
		return response;
	}

}
