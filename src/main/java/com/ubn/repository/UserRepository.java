package com.ubn.repository;

import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import com.ubn.Utility.ResponseConstant;
import com.ubn.Utility.Utils;
import com.ubn.host.RestService;
import com.ubn.model.Response;
import com.ubn.model.UserInfo;

@Repository
public class UserRepository{

	@Autowired
	RestService restService;
	
	@Resource
	HttpServletResponse httpResponse;

	@Resource
	HttpServletRequest httpRequest;
	
	BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	
@SuppressWarnings("deprecation")
public UserInfo findByUsername(String username){
		UserInfo user = null;
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		String password = httpRequest.getParameter("password");

     	JSONObject request = new JSONObject();
		request.put("accountNo", username);
	 	request.put("refcode", password);
	 	request.put("isfirst", "N");
	 	request.put("vercode", "");
		
	 
		 Response response = restService.customerLogin(request);
		 System.out.println(response.getResponseCode()+""+response.getResponseMessage());

		 
		 //Try directly with raw username and password
		if (response.getResponseCode().equals("00")) {
			  JSONObject clientdetail = new JSONObject(response.getData().toString());
			  grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		   
			  user = new UserInfo(username, bCryptPasswordEncoder.encode(password), true, true, true, true,
					grantedAuthorities, clientdetail.getString("ac_DESC"), clientdetail.getString("mobile_NUMBER"),
					clientdetail.getString("cust_AC_NO"), clientdetail.getString("referal_CODE"),
					clientdetail.getString("segment_DESC"), clientdetail.getString("e_MAIL"), "MGM_WEB");
		  }
		
		
		
		//Try to decrypt username and password and try again
		else {
			try {
			String decryptedFirstString = Utils.decryptString(URLDecoder.decode(username).replace(" ", "+"));
			String decryptedSecondString = Utils.decryptString(URLDecoder.decode(password).replace(" ", "+"));
			
			
			String[] firstPart = decryptedFirstString.split(",");
			String[] secondPart = decryptedSecondString.split(",");
			
			boolean rspBool;
			
			if(firstPart[1].equalsIgnoreCase("mgm_web")) {
				 rspBool = true;
			}
			else {
				 rspBool = Utils.checkCredentialsValidity(secondPart[1]);	
			  }
			
			if(rspBool == true) {
				JSONObject secondRequest = new JSONObject();
				secondRequest.put("accountNo", firstPart[0]);
			 	secondRequest.put("refcode", secondPart[0]);
			 	//If the source of request is from mgm source
			 	
				if (firstPart[1].equalsIgnoreCase("mgm_web")) {
					secondRequest.put("isfirst", "Y");
					secondRequest.put("vercode", secondPart[1]);
				} else {
					secondRequest.put("isfirst", "N");
					secondRequest.put("vercode", "");
				}
				
			     Response secondResponse = restService.customerLogin(secondRequest);
			     if (secondResponse.getResponseCode().equals("00")) {
					 JSONObject clientdetail = new JSONObject(secondResponse.getData().toString());
					 grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
				   
					 user = new UserInfo(firstPart[0], bCryptPasswordEncoder.encode(password), true, true, true, true,
							grantedAuthorities, clientdetail.getString("ac_DESC"), clientdetail.getString("mobile_NUMBER"),
							clientdetail.getString("cust_AC_NO"), clientdetail.getString("referal_CODE"),
							clientdetail.getString("segment_DESC"), clientdetail.getString("e_MAIL"),firstPart[1]);
				     }
				 System.out.println(secondResponse.getResponseCode()+""+secondResponse.getResponseMessage());
			}else {
				httpRequest.setAttribute("channel", firstPart[1]);
				httpRequest.setAttribute("rspCode", ResponseConstant.EXPIREDCREDENTIALS);
				httpRequest.setAttribute("redirectUri", "");
			 }
			}
			catch(Exception e) {
				//====threw a login error
			}
		   }
		return user;
	}



}
