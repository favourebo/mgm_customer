package com.ubn.model;

import java.util.Collection;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

@SuppressWarnings("serial")
public class UserInfo extends User {
	private String fullName;
    private String mobileNumber;
    private String cust_AC_NO;
    private String empNumber;
    private String segmentDesc;
    private String email;
    private String channelCode;
    private Set<String> roles;
    
    
   public UserInfo(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities, String fullName,String mobileNumber,String cust_AC_NO,
			String empNumber, String segmentDesc, String email,String channelCode) {
	
	   super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	   this.fullName = fullName;
	   this.mobileNumber = mobileNumber;
	   this.cust_AC_NO = cust_AC_NO;
	   this.empNumber = empNumber;
	   this.segmentDesc = segmentDesc;
	   this.email = email;
	   this.channelCode = channelCode;
	}

	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmpNumber() {
		return empNumber;
	}
	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Set<String> getRoles() {
		return roles;
	}
	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}


	public String getMobileNumber() {
		return mobileNumber;
	}


	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	public String getCust_AC_NO() {
		return cust_AC_NO;
	}


	public void setCust_AC_NO(String cust_AC_NO) {
		this.cust_AC_NO = cust_AC_NO;
	}


	public String getSegmentDesc() {
		return segmentDesc;
	}


	public void setSegmentDesc(String segmentDesc) {
		this.segmentDesc = segmentDesc;
	}


	public String getChannelCode() {
		return channelCode;
	}


	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}



   
  
}
