package com.ubn.model;

import java.util.List;

public class Request {
    private String accountNumber;
    private String otp;
    private String surname;
    private String relationship;
    private String firstName;
    private String captcha;
    private String notification;
    private String mobilePhone;
    private String address;
    private String email;
    private String referralCode;
    private String refCode;
    private String queryCode;
    private String statusCode;
    private String transId;
    private String staffRoleCode;
    private String status;
    private String dateOfCall;
    private String timeOfCall;
    private String venueOfCall;
    private String cityOfCall;
    private String groupHead;
    private String relationshipManager;
    private String staffId;
    private String remark;
    private String countType;
    private String totalRequestCount;
    private String categoryId;
    private String categoryName;
    private String categoryDescription;
    private String productName;
    private String productId;
    private String reportType;
    private String pointPerProduct;
    private String availabilityStatus;
    private String startDate;
    private String endDate;
    private String minPoint;
    private String verCode;
   
    public String getVerCode() {
		return verCode;
	}

	public void setVerCode(String verCode) {
		this.verCode = verCode;
	}

	private String maxPoint;
    private List<RedemptionList> redemptionList;

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public String getQueryCode() {
		return queryCode;
	}

	public void setQueryCode(String queryCode) {
		this.queryCode = queryCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDateOfCall() {
		return dateOfCall;
	}

	public void setDateOfCall(String dateOfCall) {
		this.dateOfCall = dateOfCall;
	}

	public String getTimeOfCall() {
		return timeOfCall;
	}

	public void setTimeOfCall(String timeOfCall) {
		this.timeOfCall = timeOfCall;
	}

	public String getVenueOfCall() {
		return venueOfCall;
	}

	public void setVenueOfCall(String venueOfCall) {
		this.venueOfCall = venueOfCall;
	}

	public String getCityOfCall() {
		return cityOfCall;
	}

	public void setCityOfCall(String cityOfCall) {
		this.cityOfCall = cityOfCall;
	}

	public String getGroupHead() {
		return groupHead;
	}

	public void setGroupHead(String groupHead) {
		this.groupHead = groupHead;
	}

	public String getStaffRoleCode() {
		return staffRoleCode;
	}

	public void setStaffRoleCode(String staffRoleCode) {
		this.staffRoleCode = staffRoleCode;
	}

    public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getRelationshipManager() {
		return relationshipManager;
	}

	public void setRelationshipManager(String relationshipManager) {
		this.relationshipManager = relationshipManager;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTotalRequestCount() {
		return totalRequestCount;
	}

	public void setTotalRequestCount(String totalRequestCount) {
		this.totalRequestCount = totalRequestCount;
	}

	public String getCountType() {
		return countType;
	}

	public void setCountType(String countType) {
		this.countType = countType;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPointPerProduct() {
		return pointPerProduct;
	}

	public void setPointPerProduct(String pointPerProduct) {
		this.pointPerProduct = pointPerProduct;
	}

	public String getAvailabilityStatus() {
		return availabilityStatus;
	}

	public void setAvailabilityStatus(String availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getMinPoint() {
		return minPoint;
	}

	public void setMinPoint(String minPoint) {
		this.minPoint = minPoint;
	}

	public String getMaxPoint() {
		return maxPoint;
	}

	public void setMaxPoint(String maxPoint) {
		this.maxPoint = maxPoint;
	}

	public List<RedemptionList> getRedemptionList() {
		return redemptionList;
	}

	public void setRedemptionList(List<RedemptionList> redemptionList) {
		this.redemptionList = redemptionList;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

    
}
