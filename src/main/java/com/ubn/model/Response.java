package com.ubn.model;

public class Response {
    private String responseCode;
    private String responseMessage;
    private String count;
    private Object data;
    private Object dataList;
	
    
    public Response(String responseCode, String responseMessage, Object data) {
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
		this.data = data;
	}
    
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public Object getDataList() {
		return dataList;
	}

	public void setDataList(Object dataList) {
		this.dataList = dataList;
	}

	
    
    
}
