package com.ubn.model;

public class RedemptionList {
	private String prodid;
	private String qty;
	private String  itempoint;
	private String totalpoint;
	
	public String getProdid() {
		return prodid;
	}
	public void setProdid(String prodid) {
		this.prodid = prodid;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getItempoint() {
		return itempoint;
	}
	public void setItempoint(String itempoint) {
		this.itempoint = itempoint;
	}
	public String getTotalpoint() {
		return totalpoint;
	}
	public void setTotalpoint(String totalpoint) {
		this.totalpoint = totalpoint;
	}
	
	
	
	
}

