package com.ubn.host;

import java.util.Base64;
import java.util.Date;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class TokenGenerator {
	@Value("${baseURL}" )
	private  String baseURL;
	@Value("${clientSecret}" )
	private  String clientSecret;
	@Value("${clientId}")
	private  String clientId;
	@Value("${grantType}")
	private  String grantType;
	@Value("${xUser}")
	private  String xUser;
	@Value("${xPass}")
	private  String xPass;
	
	private static Logger logger = LoggerFactory.getLogger(TokenGenerator.class.getName());
   
	//-----Setting time out for service operation
	private static ClientHttpRequestFactory clientHttpRequestFactory() {
		    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		    factory.setReadTimeout(10000);
		    factory.setConnectTimeout(10000);
		    return factory;
		} 
		
	public String getToken(){
		String accesstoken = null;
		  try{
			String plainCreds = clientId+":"+clientSecret;
			byte[] plainCredsBytes = plainCreds.getBytes();
			byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
			String base64Creds = new String(base64CredsBytes);
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.add("Authorization", "Basic " + base64Creds);
            
			// HttpEntity<String>: To get result as String.
			HttpEntity<String> entity = new HttpEntity<String>(headers);
			
            RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
			
			String TOKEN_REST_URL = baseURL 
					//+ "client_secret="
			  //+ clientSecret+ "&client_id=" + clientId 
			    + "?grant_type="
				+ grantType + "&username=" + xUser + "&password="
				+ xPass;
			
			//Send request with POST method, and Headers.
			ResponseEntity<String> response = restTemplate.exchange(TOKEN_REST_URL, HttpMethod.POST, entity,String.class);
            String result = response.getBody();

			if(result != null){
				if(result.startsWith("{")){
					JSONObject jsonObject = new JSONObject(result);
					if (jsonObject.has("access_token"))
						accesstoken = jsonObject.getString("access_token");
				  }
			 } else{
				accesstoken = null;
				logger.error(new Date()+"::Method::getToken::Response::" + response);
			}
		 } catch (Exception e) {
			logger.error(new Date()+"::Method::getToken::Response::" + e.getMessage());
		}
	 return accesstoken;
  }
}
