package com.ubn.host;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.ubn.model.Response;

@Component
public class RestService {

	@Autowired
	RestConnector restConnector;
	
	@Autowired
	TokenGenerator tokenGenerator;

	@Value("${validateCustomerAndGenerateOtpUrl}")
	private String validateCustomerAndGenerateOtpUrl;
	
	@Value("${validateOtpAndRegisterCustomerUrl}")
	private String validateOtpAndRegisterCustomerUrl;
	
	@Value("${referCustomerUrl}")
	private String referCustomerUrl;
	
	@Value("${authenticateStaffUserUrl}" )
	private String authenticateStaffUserUrl;
	
	@Value("${validateReferralCodeUrl}" )
	private String validateReferralCodeUrl;
	
	@Value("${getReferalByDeptCodeAndTxnidUrl}" )
	private String getReferalByDeptCodeAndTxnidUrl;
	
	@Value("${getAllReferedCustomerUrl}" )
	private String getAllReferedCustomerUrl;
	
	@Value("${getCustomerDetailsByTxnidUrl}" )
	private String getCustomerDetailsByTxnidUrl;
	
	@Value("${saveReferalConsentUrl}" )
	private String saveReferalConsentUrl;
	
	@Value("${saveGroupHeadRemarkUrl}" )
	private String saveGroupHeadRemarkUrl;
	
	@Value("${saveRMRemarkUrl}" )
	private String saveRMRemarkUrl;
	
	@Value("${getRecordCountUrl}" )
	private String getRecordCountUrl;
	
	@Value("${createNewProductCategoryUrl}" )
	private String  createNewProductCategoryUrl;
	
	@Value("${createNewProductUrl}" )
	private String createNewProductUrl;
	
	@Value("${getAllLocationUrl}" )
	private String getAllLocationUrl;
	
	@Value("${getAllProductCategoryUrl}" )
	private String getAllProductCategoryUrl;
	
	@Value("${getRedemptionItemsUrl}" )
	private String getRedemptionItemsUrl;
	
	@Value("${getProductByIdurl}" )
	private String getProductByIdurl;
	
	@Value("${getCategoryByIdurl}" )
	private String getCategoryByIdurl;
	
	@Value("${updateDeleteProductCategoryurl}" )
	private String updateDeleteProductCategoryurl;
	
	@Value("${updateDeleteProductUrl}" )
	private String updateDeleteProductUrl;
	
	@Value("${saveRedemptionUrl}" )
	private String saveRedemptionUrl;
	
	@Value("${getReferalByRefcodeUrl}" )
	private String getReferalByRefcodeUrl;
	
	@Value("${getCustomerReferalRecordUrl}" )
	private String getCustomerReferalRecordUrl;
	
	@Value("${referralManagementReportPrefix}" )
    private String referralManagementReportPrefix;
	
	@Value("${captchaUrl}" )
	private String captchaUrl;
	
	@Value("${secret}" )
	private String secret;
	
	@Value("${customerLoginUrl}" )
	private String customerLoginUrl;
	
	public Response validateCaptcha(String response) {
		return restConnector.validateCaptcha(captchaUrl, secret, response);
	}
	
	public Response getCustomerCount(JSONObject request) {
		return restConnector.sendPostRequest(getRecordCountUrl+tokenGenerator.getToken(), request);
	}
	
	public Response validateAccountNumber(JSONObject request) {
		return restConnector.sendPostRequest(validateCustomerAndGenerateOtpUrl+tokenGenerator.getToken(), request);
	}
	
	public Response validateCustomerAndGenerateOtp(JSONObject request) {
		return restConnector.sendPostRequest(validateCustomerAndGenerateOtpUrl+tokenGenerator.getToken(), request);
	}

	public Response validateOtpAndRegisterCustomer(JSONObject request) {
		return restConnector.sendPostRequest(validateOtpAndRegisterCustomerUrl+tokenGenerator.getToken(), request);
	}

	public Response referCustomer(JSONObject request) {
		return restConnector.sendPostRequest(referCustomerUrl+tokenGenerator.getToken(), request);
	}

	public Response authenticateStaff(JSONObject request) {
		return restConnector.sendPostRequest(authenticateStaffUserUrl+tokenGenerator.getToken(), request);
	}

	public Response validateReferralCode(JSONObject request) {
		return restConnector.sendPostRequest(validateReferralCodeUrl+tokenGenerator.getToken(), request);
	}
    
	public Response getReferalByDeptCodeAndTxnid(JSONObject request) {
		return restConnector.sendPostRequest(getReferalByDeptCodeAndTxnidUrl+tokenGenerator.getToken(), request);
	}
	
	public Response getAllReferedCustomer() {
		return restConnector.sendGetRequest(getAllReferedCustomerUrl+tokenGenerator.getToken());
	}
	
	public Response getCustomerDetailsByTxnid(JSONObject request) {
		return restConnector.sendPostRequest(getCustomerDetailsByTxnidUrl+tokenGenerator.getToken(), request);
	}
	
	public Response saveReferalConsent(JSONObject request) {
		return restConnector.sendPostRequest(saveReferalConsentUrl+tokenGenerator.getToken(), request);
	}

	public Response saveGroupHeadRemark(JSONObject request) {
		return restConnector.sendPostRequest(saveGroupHeadRemarkUrl+tokenGenerator.getToken(), request);
	}

	public Response saveRMRemark(JSONObject request) {
		return restConnector.sendPostRequest(saveRMRemarkUrl+tokenGenerator.getToken(),request);
	}

    public Response createCategory(JSONObject request) {
		return restConnector.sendPostRequest(createNewProductCategoryUrl+tokenGenerator.getToken(), request);
	}

	public Response createProduct(JSONObject request) {
		return restConnector.sendPostRequest(createNewProductUrl+tokenGenerator.getToken(), request);
	}

	public Response getAllLocation() {
		return restConnector.sendGetRequest(getAllLocationUrl+tokenGenerator.getToken());
	}

	public Response getAllCategory() {
		return restConnector.sendGetRequest(getAllProductCategoryUrl+tokenGenerator.getToken());
	}

	public Response getAllProduct(JSONObject request) {
		return restConnector.sendPostRequest(getRedemptionItemsUrl+tokenGenerator.getToken(), request);
	}

	public Response getCategoryById(JSONObject request) {
		return restConnector.sendPostRequest(getCategoryByIdurl+tokenGenerator.getToken(), request);
	}

	public Response getProductById(JSONObject request) {
		return restConnector.sendPostRequest(getProductByIdurl+tokenGenerator.getToken(), request);
	}

	public Response updateDeleteCategory(JSONObject request) {
		return restConnector.sendPostRequest(updateDeleteProductCategoryurl+tokenGenerator.getToken(), request);
	}

	public Response updateDeleteProduct(JSONObject request) {
		return restConnector.sendPostRequest(updateDeleteProductUrl+tokenGenerator.getToken(), request);
	}

	public Response generateReferralReport(String url,JSONObject request) {
		return restConnector.sendPostRequest(referralManagementReportPrefix+url+"?access_token="+tokenGenerator.getToken(), request);
	}

	public Response redeemPoint(JSONObject request) {
		return restConnector.sendPostRequest(saveRedemptionUrl+tokenGenerator.getToken(), request);
	}

	public Response getReferralByRefCode(JSONObject request) {
		return restConnector.sendPostRequest(getReferalByRefcodeUrl+tokenGenerator.getToken(),request);
	}

	public Response customerLogin(JSONObject request) {
		return restConnector.sendPostRequest(customerLoginUrl+tokenGenerator.getToken(),request);
	}

	public Response getReferralByRefCodeAndStatus(JSONObject request) {
		return restConnector.sendPostRequest(getCustomerReferalRecordUrl+tokenGenerator.getToken(), request);
	}
}
