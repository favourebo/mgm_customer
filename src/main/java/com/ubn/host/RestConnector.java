package com.ubn.host;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.ubn.Utility.Utils;
import com.ubn.model.Response;

@Component
public class RestConnector {
	 
	// -----Setting time out for service operation
	private ClientHttpRequestFactory clientHttpRequestFactory() {
			HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
			factory.setReadTimeout(100000000);
			factory.setConnectTimeout(100000000);
			return factory;
		}
	
	public Response sendPostRequest(String url, JSONObject request) {
		     // Default response
				Response response = new Response("99", "An unknown error occurred", null);
				String CONNECTION_URL = url;
				try {
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);

					
					HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);
                    RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());

					// Send request with POST method, and Headers.
					ResponseEntity<String> responseFromServer = restTemplate.exchange(CONNECTION_URL, HttpMethod.POST, entity,
							String.class);

					String result = responseFromServer.getBody();

					if (result != null) {
						if (result.startsWith("{")) {
							JSONObject jsonObject = new JSONObject(result);
							if (Utils.nullFormatter(jsonObject.get("responseCode")).equals("00")) {
								if(CONNECTION_URL.contains("authenticateStaffUser")) {
									response = new Response(Utils.nullFormatter(jsonObject.get("responseCode")), "Successful",
											jsonObject.get("data"));	
								    }
								else if(jsonObject.get("data").toString()!= null) {
									response = new Response(Utils.nullFormatter(jsonObject.get("responseCode")), "Successful",
											jsonObject.get("data").toString());	
									if(Utils.nullFormatter(jsonObject.get("count"))!= null) {
										response.setCount(Utils.nullFormatter(jsonObject.get("count")));
									}
									if(jsonObject.get("datalst").toString()!= null) {
										response.setDataList(Utils.nullFormatter(jsonObject.get("datalst")));
									}
								}
							    else {
									response = new Response(Utils.nullFormatter(jsonObject.get("responseCode")), Utils.nullFormatter(jsonObject.get("responseMessage")),
											null);	
								}
							} else {
								response = new Response("97", Utils.nullFormatter(jsonObject.get("responseMessage")), null);
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					response = new Response("100", "An error occured kindly contact your administrator", null);
					// logger.error(new Date()+"::Method::getToken::Response::" + e.getMessage());
				}
			return response;
	    }
	
    public Response sendGetRequest(String url) {
		 // Default response
		Response response = new Response("99", "An unknown error occurred", null);
		String CONNECTION_URL = url;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// HttpEntity<String>: To get result as String.
		    HttpEntity<String> entity = new HttpEntity<String>(headers);
			
		    RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());

			// Send request with GET method, and Headers.
			ResponseEntity<String> responseFromServer = restTemplate.exchange(CONNECTION_URL, HttpMethod.GET, entity,
					String.class);

			String result = responseFromServer.getBody();

			if (result != null) {
				if (result.startsWith("{")) {
					JSONObject jsonObject = new JSONObject(result);
					if (Utils.nullFormatter(jsonObject.get("responseCode")).equals("00")) {
						if(CONNECTION_URL.contains("authenticateStaffUser")) {
							response = new Response(Utils.nullFormatter(jsonObject.get("responseCode")), "Successful",
									jsonObject.get("data"));	
						    }
						else if(jsonObject.get("data").toString()!= null) {
							response = new Response(Utils.nullFormatter(jsonObject.get("responseCode")), "Successful",
									jsonObject.get("data").toString());	
							if(Utils.nullFormatter(jsonObject.get("count"))!= null) {
								response.setCount(Utils.nullFormatter(jsonObject.get("count")));
							}
						}
					    else {
							response = new Response(Utils.nullFormatter(jsonObject.get("responseCode")), "Successful",
									null);	
						}
					} else {
						response = new Response("97", Utils.nullFormatter(jsonObject.get("responseMessage")), null);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new Response("100", "An error occured kindly contact your administrator", null);
			// logger.error(new Date()+"::Method::getToken::Response::" + e.getMessage());
		}
	return response;
	}
    
    
    
public Response validateCaptcha(String url, String secret, String captchaResponse) {
    	 // Default response
		Response response = new Response("99", "An unknown error occurred", null);
		String CONNECTION_URL = url+"?secret="+secret+"&response="+captchaResponse;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// HttpEntity<String>: To get result as String.
		    HttpEntity<String> entity = new HttpEntity<String>(headers);
			
		    RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());

			// Send request with GET method, and Headers.
			ResponseEntity<String> responseFromServer = restTemplate.exchange(CONNECTION_URL, HttpMethod.GET, entity,
					String.class);

			String result = responseFromServer.getBody();

			if (result != null) {
				if (result.startsWith("{")) {
					JSONObject jsonObject = new JSONObject(result);
					if (Utils.nullFormatter(jsonObject.get("success")).equals("true")) {
						response = new Response("00", "successful", null);
					} else {
						response = new Response("97", Utils.nullFormatter(jsonObject.get("responseMessage")), null);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new Response("100", "An error occured kindly contact your administrator", null);
			// logger.error(new Date()+"::Method::getToken::Response::" + e.getMessage());
		}
	return response; 	
    }
}
