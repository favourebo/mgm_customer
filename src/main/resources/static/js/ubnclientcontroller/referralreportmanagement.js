var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));


function getReportType(){
	var reporType = $("#reportType").val();
switch(reporType){
case "REFERRAL DATE":
	  $("#startDate").show();
	  $("#startDate_L").show();
	  $("#endDate").show();
	  $("#endDate_L").show();
	  $("#min").hide();
		 $("#min_L").hide();
		 $("#max").hide();
		 $("#max_L").hide();
		 $("#statusCode_").hide();
		 $("#location").hide();
	break;
case "REFERRED CUSTOMER":
	 $("#startDate").show();
	 $("#startDate_L").show();
	 $("#endDate").show();
	 $("#endDate_L").show();
	 $("#min").hide();
	 $("#min_L").hide();
	 $("#max").hide();
	 $("#max_L").hide();
	 $("#statusCode_").show();
	 $("#location").hide();
	break;
case "ACCUMULATED POINTS BY USER":
	 $("#startDate").hide();
	 $("#startDate_L").hide();
	 $("#endDate").hide();
	 $("#endDate_L").hide();
	 $("#min").show();
	 $("#min_L").show();
	 $("#max").show();
	 $("#max_L").show();
	 $("#statusCode_").hide();
	 $("#location").hide();
	break;
case "USER LOCATION":
	 $("#startDate").hide();
	 $("#startDate_L").hide();
	 $("#endDate").hide();
	 $("#endDate_L").hide();
	 $("#min").hide();
	 $("#min_L").hide();
	 $("#max").hide();
	 $("#max_L").hide();
	 $("#location").show();
	 $("#statusCode_").hide();
	break;	
}
}

function generateReport(){
	var table = $('#xReferral_Report_table').DataTable();
	table.destroy();
	var reporType = $("#reportType").val();
	var referralReportManagementData = buildJsonFromFormData("#referralReportManagementForm");
	
	$.ajax({
		  type: "POST",
		  url: ctx+"/generateReferralReport",
		  data: referralReportManagementData,
		  dataType: "json",
		  contentType : "application/json",
		  success: function(response){
			 $("#caption").text(reporType+" REPORT");
			 var data = JSON.parse(response.data)
		    if(response.responseCode == "00"){
		      $('#xReferral_Report_table').DataTable( {
		    	"scrollCollapse": false,
		    	"bScrollCollapse": true,
                "data": data,
                "columns": [
              { "data": "firstname" },
              { "data": "lastname" },
              { "data": "phone" },
              { "data": "location" },
              { "data": "refcode" },
              { "data": "reqstatus" },
              { "data": "datecreated" }     
            ]
          } );

			  }
			   else{
				     swal(
						  'Failed Operation',
						   response.responseMessage,
						  'error'
					   ) 
			      }
			 },
		  error: function() {
			        swal(
						  'OOPS an error occured!',
						  'Kindly contact an administrator',
						  'error'
					   )
					   }
	   });
	}














	
