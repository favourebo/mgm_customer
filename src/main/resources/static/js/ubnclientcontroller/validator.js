var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));

//Converts form data to Json


function specialCharacterValidator(value){
	var rsp = false;
	if(isNaN(value)){
		rsp = false;
	 }else{
		rsp = true;
	 }
	return rsp;
}


function currencyValidator(money){
	var rsp = false;
	if(isNaN(money)){
		rsp = false;
	 }else{
		rsp = true;
	 }
	return rsp;
}

function nullValidator(input){
	var rsp = false;
 
	if(input != null) {
		if(input != undefined){
		if(input == "") {
			rsp = false;
		}else {
			rsp =  true;
		}
		}
	  }
	return rsp;
}

function lengthValidator(input,length) {
	var rsp = false;
	console.log(input);
	try{
	if (input != null) {
		if (input.length == length) {
			rsp = true;
		}
	 }
	}catch(err){
		rsp = false;
	}
	return rsp;
}

function validateAccountNumber(registerCustomerData){
	 var customerdata = JSON.parse(registerCustomerData);
	 var response = "successfully validated"
		
		 //----Validate Account Number
		    if(nullValidator(customerdata.accountNumber) == true) {
			    	if(specialCharacterValidator(customerdata.accountNumber) == false) {
			    		response = "Invalid account number, Field should contain only numbers!";	
			    		return response;
			    	}
			    	else {
			    		if(lengthValidator(customerdata.accountNumber, 10) == false) {
			    			response = "Account number should have a length of 10!";	
				    		return response;
			    		} 
		              }
		          }
			   else{
				    response = "Account Number field cannot be empty!";
			    	return response;
			   }
return response;
}

function validateCustomerReferralData(customerReferralData){
	 var customerreferraldata = JSON.parse(customerReferralData);
	 var response = "successfully validated"
		 
		
		if(nullValidator(customerreferraldata.surname) == false) {
			  response = "Surname field cannot be empty!";
		    	return response;
	         }
	 
	  if(nullValidator(customerreferraldata.firstName) == false) {
		  response = "First Name field cannot be empty!";
	    	return response;
          }
	  
	
	  
	//----Validate Account Number
	    if(nullValidator(customerreferraldata.mobilePhone) == true) {
		    	if(specialCharacterValidator(customerreferraldata.mobilePhone) == false) {
		    		console.log(customerreferraldata.mobilePhone);
		    		response = "Invalid Mobile Phone, Field should contain only numbers!";	
		    		return response;
		    	}
		    	else {
		    		if(lengthValidator(customerreferraldata.mobilePhone, 11) == false) {
		    			response = "Invalid Mobile Phone Length!";	
			    		return response;
		    		} 
	              }
	          }
		   else{
			    response = "Mobile Phone cannot be empty!";
		    	return response;
		   }
	    
	 if(nullValidator(customerreferraldata.address) == false) {
		  response = "Address field cannot be empty!";
	    	return response;
        }
	 
	 
	 if(customerreferraldata.relationship == "d" ||customerreferraldata.relationship == "b" || customerreferraldata.relationship == "s"
		 || customerreferraldata.relationship == "m" ||  customerreferraldata.relationship == "f"
	 ){
		 response = "You cannot refer your Brother,Sister,Father,Mother!"; 
	 }
	  else if(customerreferraldata.relationship == "" ){
		  response = "Please select a relationship !";
	      return response;  
	 }
	 
	 if(nullValidator(customerreferraldata.email) == true) {
		 if(customerreferraldata.email.includes("@")){
		  response = "successfully validated";
       } else{
    	   response = "Invalid email address format!";
	    	return response;  
       }  
	 }
	 else{
		 response = "Email field cannot be empty!";
	    	return response;
	 }
	 
	 
	
	  
	 
	 return response
}



function xxData(xxdata){
	var response = null;
	var xRequest = {"xxData":xxdata}
	$.ajax({
		  type: "POST",
		  url: ctx+"/",
		  data: JSON.stringify(xRequest),
		  dataType: "json",
		  async:false,
		  contentType : "application/json",
		  success: function(data){
			  response =  data.toString
		  }
	   });
	return response;
}







function validateCustomerRegistrationDetails(registerCustomerData){
	 var customerdata = JSON.parse(registerCustomerData);
	 var response = "successfully validated"
		
		 //----Validate Account Number
		    if(nullValidator(customerdata.accountNumber) == true) {
			    	if(specialCharacterValidator(customerdata.accountNumber) == false) {
			    		response = "Invalid account number, Field should contain only numbers!";	
			    		return response;
			    	}
			    	else {
			    		if(lengthValidator(customerdata.accountNumber, 10) == false) {
			    			response = "Account number should have a length of 10!";	
				    		return response;
			    		} 
		              }
		          }
			   else{
				    response = "Account Number field cannot be empty!";
			    	return response;
			   }
		 
     //----Validate otp code
	  /*if(nullValidator(customerdata.otp) == true) {
	  	if(specialCharacterValidator(customerdata.otp) == false) {
	  		response = "Invalid OTP, Field should contain only numbers!";	
	  	  return response;
	  	} 
	  	else {
	  		if(customerdata.otp.length < 4) {
	  			response = "otp cannot have a length less than 4!";	
		    	return response;
	  		}
	  	   //Validate Existence Of EnvelopeId
	  	   }
	    }
	     else {
	    	 response = "OTP field cannot be empty!";
	  	      return response;
	   }
		*/
	 
	 
	  return response;
}
