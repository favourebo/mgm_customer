var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));




function buildJsonFromFormData(selector){
	 var ary = $(selector).serializeArray();
	 var obj = {};
	 for (var a = 0; a < ary.length; a++) obj[ary[a].name] = ary[a].value;
	 return JSON.stringify(obj);
}



//This resets the registration form
function doReset(){
	$("#submitRegisteredCustomerbtn").hide();
	$("#otp_m").hide();
	$("#accountName").hide();
	$("#phoneNumber").hide();
	$("#emailAddress").hide();
	$("#notification").hide();
	$("#validateAccountNumberBtn").show();
	$("#accountNumber").val("");
	$("#otp").val("");
}



function confirmRegDetailsBeforeSubmission(){
	var registerCustomerData = buildJsonFromFormData("#registerCustomerForm");
	var validationRsp = validateCustomerRegistrationDetails(registerCustomerData);
	if(validationRsp.startsWith("successful")){
		 swal({
		  title: 'Confirmation Message',
		  text: 'Are you sure you want to create this customer on the MGM platform?',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No'
		}).then((result) => {
		  if (result.value) {
			  validateOtpAndRegisterCustomer(registerCustomerData);
		  }
		  else{
			 // doReset();
		}
})
	}else{
		  swal(
				  'Failed Validation Response',
				  validationRsp,
				  'error'
			   )
	}
}	




function validateAccountNumberAndSendOtp(){
	var registerCustomerData = buildJsonFromFormData("#registerCustomerForm");
	var validationRsp = validateAccountNumber(registerCustomerData);
	if(validationRsp.startsWith("successful")){
	          $.ajax({
					  type: "POST",
					  url: ctx+"/validate-accountnumber-and-generate-otp",
					  data: registerCustomerData,
					  dataType: "json",
					  contentType : "application/json",
					  beforeSend: function(){
						  $("#regcustomerpreloader").show();
					    },
					  complete: function(){
						 $("#regcustomerpreloader").hide();
					    },
					  success: function(response){
						  console.log(response)
						   var data = JSON.parse(response.data)
						  if(response.responseCode == "00"){
							    $("#validateAccountNumberBtn").hide();
								$("#submitRegisteredCustomerbtn").show(1000);
								$("#accountName").show(1000);
								$("#phoneNumber").show(1000);
								$("#emailAddress").show(1000);
								$("#otp_m").show(1000);
								$("#notification").show(1000);
								if(data.mobile_NUMBER == null || data.mobile_NUMBER == ""){
									$("#xphoneNumber").val("Information Not Available");	
								}else{
									$("#xphoneNumber").val(data.mobile_NUMBER);	
								}
							    $("#xaccountName").val(data.ac_DESC);
								if(data.e_MAIL == null || data.e_MAIL == ""){
									$("#xemailAddress").val("Information Not Available");	
								}else{
									$("#xemailAddress").val(data.e_MAIL);	
								}
								swal(
										  'Successful Validation',
										  'Account number validation was successful',
										  'success'
									   ) 
							   }
						  else{
							    swal(
									  'Failed Operation',
									  response.responseMessage,
									  'error'
								   ) 
						  }
						 },
					  error: function() {
						        swal(
									  'OOPS an error occured!',
									  'Kindly contact an administrator',
									  'error'
								   )
						  // setTimeout(function(){window.location.href = '/offline_envelope_deposit'  }, 2000);
			              }
				   });
	}else{
		  swal(
				  'Failed Validation Response',
				  validationRsp,
				  'error'
			   )
	}
	}




function validateOtpAndRegisterCustomer(registerCustomerData){
	$.ajax({
			  type: "POST",
			  url: ctx+"/validate-otp-and-register-customer",
			  data: registerCustomerData,
			  dataType: "json",
			  contentType : "application/json",
			  beforeSend: function(){
				  $("#regcustomerpreloader").show();
			    },
			  complete: function(){
				 $("#regcustomerpreloader").hide();
			    },
			  success: function(response){
				  console.log(response)
				  if(response.responseCode == "00"){
					  swal({
						  title: 'Customer Registration was successful',
						  type: 'success',
						 text: 'Referral code has been sent to customers registered mobile'
						}); 
					 setTimeout(function(){window.location.href = '/home'}, 3000);
				   }
				  
				  else{
					    swal(
							  'Failed Operation',
							  response.responseMessage,
							  'error'
						   ) 
				     }
				 },
			  error: function() {
				        swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )
				  // setTimeout(function(){window.location.href = '/offline_envelope_deposit'  }, 2000);
	              }
		   });
}

$('#xDoRegistration').on('hidden.bs.modal', function () {
	doReset();
	});