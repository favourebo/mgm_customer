var xxrefcode = "";
var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));

function doRefCodeValidationReset(){
	$("#refCodeValidationTable").hide();
	$("#name").text("");
	$("#phone").text("");
	$("#accountnumber").text("");
	$("#segmentDesc").text("");
	$("#refCode").val("");
}

function getReferralHistory(xxrefcode,statuscode){
	var table = $('#xxreferral_list_table').DataTable();
	table.clear();
	table.destroy();
	var request = {"refCode":xxrefcode,"statusCode":statuscode};
	$.ajax({
		 type: "POST",
		  url: ctx+"/getReferralByRefCodeAndStatus",
		  data: JSON.stringify(request),
		  dataType: "json",
		  contentType : "application/json",
		  success: function(response){
			var data = JSON.parse(response.data)
		   if(response.responseCode == "00"){
			if(statuscode == "O"){
				$("#statCode").text("REFERRAL HISTORY FOR PENDING");	
			}else if(statuscode == "N"){
				$("#statCode").text("REFERRAL HISTORY FOR COMPLETED");	
			}
			else if(statuscode == "D"){
				$("#statCode").text("REFERRAL HISTORY FOR DECLINED");	
			}else{
				$("#statCode").text("REFERRAL HISTORY FOR ALL");
			}
			
		      $('#xxreferral_list_table').DataTable( {
		    	"scrollCollapse": true,
		    	"bScrollCollapse": true,
	          "data": data,
	        "columns": [
	        { "data": "firstname" },
	        { "data": "lastname" },
	        { "data": "phone" },
	        { "data": "location" }, 
	        { "data": "email" },
	        { "data": "reqstatus"},
	        { "data": "datecreated"},
	      ]
	      } );
		    $("#xyrefcode").text(xxrefcode);
	       }
			   else{
				     swal(
						  'Failed Operation',
						   response.responseMessage,
						  'error'
					   ) 
			      }
			 },
		  error: function() {
			        swal(
						  'OOPS an error occured!',
						  'Kindly contact an administrator',
						  'error'
					   )
					   }
	 });
}


$('#xReferralListModal').on('hidden.bs.modal', function () {
	var tables = $('#xxreferral_list_table').DataTable();
	tables.destroy();
	});

$('#xRefCodeValidation').on('hidden.bs.modal', function () {
	doRefCodeValidationReset();
	});


function validateRefCode(){
	doRefCodeValidationReset();
	var refCodeValidationData = buildJsonFromFormData("#refCodeValidationForm");
	var xformattedrefCodeValidationData = JSON.parse(refCodeValidationData);
	xxrefcode = xformattedrefCodeValidationData.refCode;
	   $.ajax({
			  type: "POST",
			  url: ctx+"/validate-referral-code",
			  data: refCodeValidationData,
			  dataType: "json",
			  contentType : "application/json",
			  beforeSend: function(){
				  $("#refcodevalidationpreloader").show();
			    },
			  complete: function(){
				 $("#refcodevalidationpreloader").hide();
			    },
			  success: function(response){
				   var data = JSON.parse(response.data)
				   
				  if(response.responseCode == "00"){
						$("#refCodeValidationTable").show(1000);
						$("#name").text(data.ac_DESC);
						$("#phone").text(data.mobile_NUMBER);
						$("#accountnumber").text(data.cust_AC_NO);
						$("#segmentDesc").text(data.segment_DESC);
						$("#xxpoint").text(data.point);
						$("#xsrefcode").text(xxrefcode);
					 } 
				  else{
					    swal(
							  'Failed Operation',
							  response.responseMessage,
							  'error'
						   ) 
				  }
				 },
			  error: function() {
				        swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )
				  // setTimeout(function(){window.location.href = '/offline_envelope_deposit'  }, 2000);
	              }
		   });
}