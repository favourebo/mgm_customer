var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));


function
initializeApp(refCode){
	
	validateRefCode(refCode);
	getReferralList(refCode);
	drawGraph(refCode);
	getCustomerPercentageCount(refCode);

}


var xCount = null;
var myData = [0,0,0];



function getRequestCount(counttype,refcode,status){
	var request = {"countType":counttype,"refCode":refcode, "statusCode":status};
	
	     $.ajax({
			  type: "POST",
			  url: ctx+"/get-analytics-info",
			  data: JSON.stringify(request),
			  dataType: "json",
			  contentType : "application/json",
			  async:false,
			  success: function(response){
				 if(response.responseCode == "00"){
					 if(response.data.pending){
						 myData[0] = parseInt(response.data.pending);
					 } if(response.data.approved){
						 myData[1] = parseInt(response.data.approved);
					 }if(response.data.declined){
						 myData[2] = parseInt(response.data.declined);
					 }
				    }
				
				 },
			  error: function() {
				      /*  swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )*/
						   }
		   });
}

function drawGraph(xxrefcode){
        
	getRequestCount("ordinary",xxrefcode,"ALL");
	//getPercentageCount("percentage","xxrefcode","ALL");
	
	var myColor = ["#FF8C00","#006400","#FF0000"];
	var myLabel = ["PENDING","APPROVED","DECLINED"];
	
	function getTotal(){
	    var myTotal = 0;
	    for (var j = 0; j < myData.length; j++) {
	       myTotal += (typeof myData[j] == 'number') ? myData[j] : 0;
	  }
	  return myTotal;
	}

	function plotData(){
	  var canvas;
	  var ctx;
	  var lastend = 0;
	  var myTotal = getTotal();
	  var doc;
	  canvas = document.getElementById("canvas");
	  var x = (canvas.width)/2;
	  var y = (canvas.height)/2;
	  var r = 150;
	  
	  ctx = canvas.getContext("2d");
	  ctx.clearRect(0, 0, canvas.width, canvas.height);

	  for (var i = 0; i < myData.length; i++) {
	    ctx.fillStyle = myColor[i];
	    ctx.beginPath();
	    ctx.moveTo(x,y);
	    ctx.arc(x,y,r,lastend,lastend+(Math.PI*2*(myData[i]/myTotal)),false);
	    ctx.lineTo(x,y);
	    ctx.fill();
	    
	    // Now the pointers
	    ctx.beginPath();
	    var start = [];
	    var end = [];
	    var last = 0;
	    var flip = 0;
	    var textOffset = 0;
	    var precentage = (myData[i]/myTotal)*100;
	    start = getPoint(x,y,r-20,(lastend+(Math.PI*2*(myData[i]/myTotal))/2));
	    end = getPoint(x,y,r+20,(lastend+(Math.PI*2*(myData[i]/myTotal))/2));
	    if(start[0] <= x)
	    {
	      flip = -1;
	      textOffset = -110;
	    }
	    else
	    {
	      flip = 1;
	      textOffset = 10;
	    }
	    ctx.moveTo(start[0],start[1]);
	    ctx.lineTo(end[0],end[1]);
	    ctx.lineTo(end[0]+120*flip,end[1]);
	    ctx.strokeStyle = "#bdc3c7";
	    ctx.lineWidth   = 2;
	    ctx.stroke();
	    // The labels
	    ctx.font="17px Arial";
	    ctx.fillText(myLabel[i]+" "+precentage.toFixed(2)+"%",end[0]+textOffset,end[1]-4); 
	    // Increment Loop
	    lastend += Math.PI*2*(myData[i]/myTotal);
	    
	  }
	}
	// Find that magical point
	function getPoint(c1,c2,radius,angle) {
	  return [c1+Math.cos(angle)*radius,c2+Math.sin(angle)*radius];
	}
	// The drawing
	plotData();	

	}

function getCustomerPercentageCount(xxrefcode){
	var request = {"countType":"percentage","refCode":xxrefcode, "statusCode":"ALL"};

    $.ajax({
		  type: "POST",
		  url: ctx+"/get-analytics-info",
		  data: JSON.stringify(request),
		  dataType: "json",
		  contentType : "application/json",
		  async:false,
			  success: function(response){
				 if(response.responseCode == "00"){
					 var p = 0;
			
			
			if(!response.data.pending && !response.data.approved && !response.data.declined){
				document.getElementById("x_per_request").style.height = "0px";
				$("#total_request").text("0%");
			}	
			try{
		    if(parseInt(response.data.pending) == 0 && parseInt(response.data.approved) == 0 && parseInt(response.data.declined) == 0)	
			 {
			    document.getElementById("x_per_request").style.height = "0px";
				$("#total_request").text("0%");
			  }		 
			}catch(err){
			  console.log(err.message)	
			}
			
			 if(response.data.pending){
					$("#pending_barchart").text(response.data.pending+"%");
					 myData[0] = parseInt(response.data.pending);
				     p = 250*parseInt(response.data.pending)
				     p = p/100; 
				     document.getElementById("x_per_pending").style.height = p.toString()+"px";
				}else{
					  $("#pending_barchart").text("0%");
					   p = p/100;
					   document.getElementById("x_per_pending").style.height = p.toString()+"px";
				}
				
				 var a = 0;
				
				 if(response.data.approved){
					$("#approved_barchart").text(response.data.approved+"%");
					myData[1] = parseInt(response.data.approved);
					 a = 250*parseInt(response.data.approved)
				     a = a/100;
					 document.getElementById("x_per_approved").style.height = a.toString()+"px";
				}else{
					 $("#approved_barchart").text("0%");
				     a = a/100;
				     document.getElementById("x_per_approved").style.height = a.toString()+"px";
				    }  
				
				var d = 0;
				if(response.data.declined){
					 $("#declined_barchart").text(response.data.declined+"%");
					 myData[2] = parseInt(response.data.declined);
					 d = 250*parseInt(response.data.declined)
				     d = d/100;
				     document.getElementById("x_per_declined").style.height = d.toString()+"px";     
				}else{
					$("#declined_barchart").text("0%");
					d = d/100;
				    document.getElementById("x_per_declined").style.height = d.toString()+"px";
				} 	  
				 }
				 else{
					   /*  swal(
							  'Failed Operation',
							   response.responseMessage,
							  'error'
						   ) */
				      }
				 },
			  error: function() {
				        /*swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )*/
						   }
		   });
}







//Validates user refcode
function validateRefCode(refCode){
	var request = {"refCode":refCode}
	   $.ajax({
			  type: "POST",
			  url: ctx+"/validate-referral-code",
			  data: JSON.stringify(request),
			  dataType: "json",
			  contentType : "application/json",
			  async: false,
			  success: function(response){
				   var data = JSON.parse(response.data)
				   
				  if(response.responseCode == "00"){
					  if(data.point == null){
						  $("#xrefpoint").text("0");  
					  }else{
						  $("#xrefpoint").text(data.point);   
					  }
					     
					 } 
				  /*else{
					    swal(
							  'Failed Operation',
							  response.responseMessage,
							  'error'
						   ) 
				  }*/
				 },
			  error: function() {
				       /* swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )*/
				  // setTimeout(function(){window.location.href = '/offline_envelope_deposit'  }, 2000);
	              }
		   });
}


//get referral list count
function getReferralList(xxrefcode){
	var request = {"refCode":xxrefcode,"statusCode":"ALL"};
	   $.ajax({
			  type: "POST",
			  url: ctx+"/getReferralByRefCodeAndStatus",
		  data: JSON.stringify(request),
		  dataType: "json",
		  contentType : "application/json",
		  success: function(response){
			var data = JSON.parse(response.data)
		if(response.responseCode == "00"){
		     var size = data.length; 
		     $("#xSize").text(size);
	       }
			  /* else{
				     swal(
						  'Failed Operation',
						   response.responseMessage,
						  'error'
					   ) 
			      }*/
			 },
		  error: function() {
			        /*swal(
						  'OOPS an error occured!',
						  'Kindly contact an administrator',
						  'error'
					   )*/
					   }
	 });
}






























/*
var xCount = null;
var myData = [0,0,0];

function intializeApp(dept,staffid){
	console.log(staffid);
	    
	  getAllReferredCustomerCount();
		if(dept == "CC"){
	      getCustomer_Count(dept,"");
		  getCustomerPercentageCount(dept,"");
		}
		
		 else if(dept == "ADMIN" || dept == "HSSA"){
			  getCustomer_Count(dept,"");
			  getCustomerPercentageCount(dept,"");
         }

		 else{
		  getCustomer_Count(dept,staffid);
		  getCustomerPercentageCount(dept,staffid);	
		 }
		
		var myColor = ["#FF8C00","#006400","#FF0000"];
		var myLabel = ["PENDING","APPROVED","DECLINED"];
		
		function getTotal(){
		  var myTotal = 0;
		  for (var j = 0; j < myData.length; j++) {
		    myTotal += (typeof myData[j] == 'number') ? myData[j] : 0;
		  }
		  return myTotal;
		}

		function plotData(){
		  var canvas;
		  var ctx;
		  var lastend = 0;
		  var myTotal = getTotal();
		  var doc;
		  canvas = document.getElementById("canvas");
		  var x = (canvas.width)/2;
		  var y = (canvas.height)/2;
		  var r = 150;
		  
		  ctx = canvas.getContext("2d");
		  ctx.clearRect(0, 0, canvas.width, canvas.height);

		  for (var i = 0; i < myData.length; i++) {
		    ctx.fillStyle = myColor[i];
		    ctx.beginPath();
		    ctx.moveTo(x,y);
		    ctx.arc(x,y,r,lastend,lastend+(Math.PI*2*(myData[i]/myTotal)),false);
		    ctx.lineTo(x,y);
		    ctx.fill();
		    
		    // Now the pointers
		    ctx.beginPath();
		    var start = [];
		    var end = [];
		    var last = 0;
		    var flip = 0;
		    var textOffset = 0;
		    var precentage = (myData[i]/myTotal)*100;
		    start = getPoint(x,y,r-20,(lastend+(Math.PI*2*(myData[i]/myTotal))/2));
		    end = getPoint(x,y,r+20,(lastend+(Math.PI*2*(myData[i]/myTotal))/2));
		    if(start[0] <= x)
		    {
		      flip = -1;
		      textOffset = -110;
		    }
		    else
		    {
		      flip = 1;
		      textOffset = 10;
		    }
		    ctx.moveTo(start[0],start[1]);
		    ctx.lineTo(end[0],end[1]);
		    ctx.lineTo(end[0]+120*flip,end[1]);
		    ctx.strokeStyle = "#bdc3c7";
		    ctx.lineWidth   = 2;
		    ctx.stroke();
		    // The labels
		    ctx.font="17px Arial";
		    ctx.fillText(myLabel[i]+" "+precentage.toFixed(2)+"%",end[0]+textOffset,end[1]-4); 
		    // Increment Loop
		    lastend += Math.PI*2*(myData[i]/myTotal);
		    
		  }
		}
		// Find that magical point
		function getPoint(c1,c2,radius,angle) {
		  return [c1+Math.cos(angle)*radius,c2+Math.sin(angle)*radius];
		}
		// The drawing
		plotData();	
}






function getAllReferredCustomerCount(){
	$.ajax({
		  type: "GET",
		  url: "/get-all-refered-customer",
		  dataType: "json",
		  contentType : "application/json",
		  async:false,
		  success: function(response){
		if(response.responseCode == "00"){
			xCount = response.count;
			$("#all_txt").text(response.count);
			  }
			   else{
				     swal(
						  'Failed Operation',
						   response.responseMessage,
						  'error'
					   ) 
			      }
			 },
		  error: function() {
			        swal(
						  'OOPS an error occured!',
						  'Kindly contact an administrator',
						  'error'
					   )
					   }
	   });
	}








function getCustomerCount(dept,status,table,staffId){
	var request = {"queryCode":dept, "statusCode":status, "staffId":staffId};
	     $.ajax({
			  type: "POST",
			  url: "/get-referral-by-deptcode-and-status",
			  data: JSON.stringify(request),
			  dataType: "json",
			  contentType : "application/json",
			  success: function(response){
				 if(response.responseCode == "00"){
					  var counter = null;
				if(table == "pending"){	
					$("#pending_txt").text(response.count);
				}
				 else if(table == "approved"){	
					 $("#approved_txt").text(response.count);		 
				 }
				 else if(table == "declined"){
					 $("#declined_txt").text(response.count);		
				 }
				     }
				 else{
					     swal(
							  'Failed Operation',
							   response.responseMessage,
							  'error'
						   ) 
				      }
				 },
			  error: function() {
				        swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )
						   }
		   });
}


function getCustomer_Count(dept,staffId){
	// xCount = $("#all_txt").text();
	var request = {"queryCode":dept,"staffId":staffId, "totalRequestCount":xCount,"countType":"ordinary"};
	 console.log(request)
	$.ajax({
			  type: "POST",
			  url: "/get-analytics-info",
			  data: JSON.stringify(request),
			  dataType: "json",
			  async:false,
			  contentType : "application/json",
			  success: function(response){
				  console.log(response)
			if(response.responseCode == "00"){
			    if(response.data.pending){
			    	$("#pending_txt").text(response.data.pending);
			    }
			    else{
			    	$("#pending_txt").text("0"); 
				}
				
				
				 if(response.data.approved){
					 console.log(response.data.approved)
				    $("#approved_txt").text(response.data.approved);	
				 }
				else{
					 $("#approved_txt").text("0");	
				    }  
				
				if(response.data.declined){     
					 $("#declined_txt").text(response.data.declined);
				}else{
					 $("#declined_txt").text("0");
				} 	  
				 
			  }
				 else{
					     swal(
							  'Failed Operation',
							   response.responseMessage,
							  'error'
						   ) 
				      }
				 },
			  error: function() {
				        swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )
						   }
		   });

}




function getCustomerPercentageCount(dept,staffId){
	// xCount = $("#all_txt").text();
	var request = {"queryCode":dept,"staffId":staffId, "totalRequestCount":xCount,"countType":"percentage"};
	 console.log(request)
	$.ajax({
			  type: "POST",
			  url: "/get-analytics-info",
			  data: JSON.stringify(request),
			  dataType: "json",
			  async:false,
			  contentType : "application/json",
			  success: function(response){
				 if(response.responseCode == "00"){
					 var p = 0;
				if(response.data.pending){
					$("#pending_barchart").text(response.data.pending+"%");
					 myData[0] = parseInt(response.data.pending);
				     p = 250*parseInt(response.data.pending)
				     p = p/100; 
				     document.getElementById("x_per_pending").style.height = p.toString()+"px";
				}else{
					  $("#pending_barchart").text("0%");
					   p = p/100;
					   document.getElementById("x_per_pending").style.height = p.toString()+"px";
				}
				
				 var a = 0;
				
				 if(response.data.approved){
					$("#approved_barchart").text(response.data.approved+"%");
					myData[1] = parseInt(response.data.approved);
					 a = 250*parseInt(response.data.approved)
				     a = a/100;
					 document.getElementById("x_per_approved").style.height = a.toString()+"px";
				}else{
					 $("#approved_barchart").text("0%");
				     a = a/100;
				     document.getElementById("x_per_approved").style.height = a.toString()+"px";
				    }  
				
				var d = 0;
				if(response.data.declined){
					 $("#declined_barchart").text(response.data.declined+"%");
					 myData[2] = parseInt(response.data.declined);
					 d = 250*parseInt(response.data.declined)
				     d = d/100;
				     document.getElementById("x_per_declined").style.height = d.toString()+"px";     
				}else{
					$("#declined_barchart").text("0%");
					d = d/100;
				    document.getElementById("x_per_declined").style.height = d.toString()+"px";
				} 	  
				 }
				 else{
					     swal(
							  'Failed Operation',
							   response.responseMessage,
							  'error'
						   ) 
				      }
				 },
			  error: function() {
				        swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )
						   }
		   });
}
*/