var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));


    //duration of checkin for expiration
	var sess_pollinterval = 2000;

	//session validity
	var sess_expirationMinutes = 5;

	//duration to alert warning prompt
	var sess_warningMinutes = 4;

	var sess_intervalID;
	var sess_lastActivity;

	$(document).ready(function() {
		initSessionMonitor();	
	});

	function initSessionMonitor(){
		sess_lastActivity = new Date();
		
		sessSetInterval();
		$(document).bind('keypress session', function(ed,e){sessKeyPressed(ed,e);})
	}

	function sessSetInterval(){
		sess_intervalID = setInterval('sessInterval()',sess_pollinterval);
	}

	function sessClearInterval(){
		clearInterval(sess_intervalID);
	}

	function sessKeyPressed(){
		sess_lastActivity = new Date();
	}

	function sessPingServer(){
		$.ajax({
			  type: "GET",
			  url: ctx+"/echo",
			  dataType: "json",
			  contentType : "application/json",
			  success: function(response){
				
				 },
			  error: function() {
			 }
		   });
	}

	function sessLogOut(){
		$.ajax({
			  type: "GET",
			  url: ctx+"/logout",
			  success: function(response){
				window.location=ctx+"/logout";
				 },
			  error: function() {
			 }
		   });
	}

	function getDiffMins(){
	   now = new Date();
	   var diff = now - sess_lastActivity;	
	   var difMins = (diff/1000/60);
	   return difMins;
	}

	function sessInterval(){
	   var difMins = getDiffMins();
	   
	   if(difMins >= sess_warningMinutes){
		   //stop timer
		  sessClearInterval();
		  //prompt for attention
		  	 swal({
			  title: 'Session Timout',
			  text: 'Your session will expire in '+(sess_expirationMinutes -sess_warningMinutes)
	                +' minutes',
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes',
			  cancelButtonText: 'No'
			}).then((result) => {
			 if (result.value) {
			     x = getDiffMins();
			     if(x > sess_expirationMinutes){
			    	 sessLogOut(); 
			     }else{
				  sessPingServer();
		          sessSetInterval();
		          sess_lastActivity = new Date();
			     }
			  }
			  else{
				sessLogOut();   
			}
	})
	 }else{
		//sessPingServer();  
	   }
	 }
