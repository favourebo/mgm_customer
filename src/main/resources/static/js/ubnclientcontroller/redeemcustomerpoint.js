var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));

var cartList = [];
var cartCount = 0;
var refCode = "";



function doRefCodeValidationReset(){
	$("#refCodeValidationTable").hide();
	$("#name").text("");
	$("#phone").text("");
	$("#accountnumber").text("");
	$("#segmentDesc").text("");
	
}


$('#xRedeemCustomerPoint').on('hidden.bs.modal', function () {
	$("#redeemcustomerpointTable").hide();
	 var table1 = $('#redeemable_item_table').DataTable();
	 table1.destroy();
	 $("#refCode6").val("");
	});



function confirmRedeemPoint(){
	 swal({
	  title: 'Confirmation Message',
	  text: 'Are you sure you want to redeem point?',
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes',
	  cancelButtonText: 'No'
	}).then((result) => {
	  if (result.value) {
		  doRedeemPoint();
	  }
	  else{
		  
	}
})
}




function doRedeemPoint(){
  //Building my request for submission	
	var data = [];
	var i =0;
	for(var i in cartList) {
	    var item = cartList[i];
          data.push({
        	"prodid" : item.product_ID,
	        "qty" : item.quantityVal,
	        "itempoint"  : item.product_POINT_VALUE,
	        "totalpoint"       : item.totalPoint,
	    });
	 }
	 var staffRoleCode = $("#staffRoleCode2").val();
	 var request = {"staffRoleCode":staffRoleCode,"refCode":refCode,"redemptionList":data};
	 $.ajax({
		  type: "POST",
		  url: ctx+"/redeemPoint",
		  data: JSON.stringify(request),
		  dataType: "json",
		  contentType : "application/json",
		  beforeSend: function(){
			  $("#cartinformationpreloader").show();
		    },
		  complete: function(){
			 $("#cartinformationpreloader").hide();
		    },
		  success: function(response){
			   var data = JSON.parse(response.data)
			   
			  if(response.responseCode == "00"){
				  afterRedeemPointSuccess();
				   swal(
						  'Successful Operation',
						  response.responseMessage,
						  'success'
					   ) 
				 }
			  else{
				    swal(
						  'Failed Operation',
						  response.responseMessage,
						  'error'
					   ) 
			  }
			 },
		  error: function() {
			        swal(
						  'OOPS an error occured!',
						  'Kindly contact an administrator',
						  'error'
					   )
			  // setTimeout(function(){window.location.href = '/offline_envelope_deposit'  }, 2000);
             }
	   });
}



function validateRefsCode(xxrefcode){
	 var request = {"refCode":xxrefcode}
	    $.ajax({
			  type: "POST",
			  url: ctx+"/validate-referral-code",
			  data: JSON.stringify(request),
			  dataType: "json",
			  contentType : "application/json",
			  beforeSend: function(){
				  $("#redeemcustomerpointpreloader").show();
			    },
			  complete: function(){
				 $("#redeemcustomerpointpreloader").hide();
			    },
			  success: function(response){
				   var data = JSON.parse(response.data)
				   redeemPointReset();
				 if(response.responseCode == "00"){
					 
						$("#redeemcustomerpointTable").show(1000);
						$("#xname").text(data.ac_DESC);
						$("#xsegmentDesc").text(data.segment_DESC);
						if(data.point == null){
							$("#xpoint").text("0");
						}
						else{
							$("#xpoint").text(data.point);
							$("#customerRemainingPoint").text(data.point);
						}
					 }
				  else{
					    swal(
							  'Failed Operation',
							  response.responseMessage,
							  'error'
						   ) 
				  }
				 },
			  error: function() {
				        swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )
				  // setTimeout(function(){window.location.href = '/offline_envelope_deposit'  }, 2000);
	              }
		   });
}







function showCartInformation(){
	if(cartList == []){
		alert("You have no pending cart list!");
	}else{
		//console.log(cartList);
		var table =  $('#xcart_table').DataTable();
		table.destroy();
    $('#xcart_table').DataTable({
    	 "paging": false,
    	 "bFilter": false,
         "ordering": false,
         "searching": false,
         "bInfo" : false,
      "data": cartList,
      "columns": [
    { "data": "product_NAME" },
    { "data": "product_POINT_VALUE" },
    { "data": "quantityVal" }, 
    { "data": "totalPoint" },
  ]
} );
	}
}

function addProduct(rowid){
	var remainingPoint = $("#customerRemainingPoint").text();
	var xrowid = parseFloat(rowid)-1;
	var data = null;
	var table = $('#redeemable_item_table').DataTable();
	var xbuttonId = "c"+rowid;
	
	//pick input value from table
	var quantityId = "#w"+rowid;
	var xquantityId = "w"+rowid;
	var totalpointId = "#t"+rowid;
	var cartJsonArrayId = "#j"+rowid;
	var buttonId = "b"+rowid;
	var quantityVal = $(quantityId).val();
	
	if(quantityVal == 0 || quantityVal < 0){
		    swal(
				  'Failed Operation',
				  'Selected quantity value must be greater than 0',
				  'error'
			   ) 
	}
	
	else{
	  var data = table.row(xrowid).data();	
	  var pointperval = data.product_POINT_VALUE;
	  var total = parseFloat(pointperval)*parseFloat(quantityVal);
	  var remainingPoint =  parseFloat(remainingPoint)- parseFloat(total);
	
	  if(remainingPoint < 0){
		  swal(
				  'Failed Operation',
				  'You have exceeded your redeemable point limit by '+ remainingPoint,
				  'error'
			   )
	  }else{
	   data.quantityVal = quantityVal;
	   data.totalPoint = total;
	   $("#customerRemainingPoint").text(remainingPoint);
	   $(totalpointId).val(total);

	  //add data to cartList
	  cartList.push(data);
	  $(cartJsonArrayId).val(cartList.length-1);
	  cartCount = cartCount+1;
	  $("#cartcount").text(cartCount);
	  document.getElementById(buttonId).disabled = true;
	  document.getElementById(xquantityId).disabled = true; 
	  document.getElementById(xbuttonId).disabled = false; 
	  }
	}
}

function removProduct(rowid){
	var buttonId = "b"+rowid;
	var xquantityId = "w"+rowid;
	var totalpointId = "#t"+rowid;
	var xbuttonId = "c"+rowid;
	var cartJsonArrayId = "#j"+rowid;
	
	//Decrements Cart counter
	if(cartCount > 0){
	 cartCount = cartCount-1;
	}
	 $("#cartcount").text(cartCount);
	 
	 // add total point back to user's redeemable point
	 var t = $(totalpointId).val(); 
	 var remainingPoint = $("#customerRemainingPoint").text();
	 var xremainingPoint = parseFloat(t) + parseFloat(remainingPoint);
	 $("#customerRemainingPoint").text(xremainingPoint);
	 
	 var xcartindex = $(cartJsonArrayId).val();
	 //Remove element from cart
	cartList.splice(xcartindex,1);
	
	//Enable button id
	document.getElementById(buttonId).disabled = false;
	document.getElementById(xquantityId).disabled = false; 
	//disable  cancel button
	document.getElementById(xbuttonId).disabled = true; 
	
	

}


function redeemPointReset(){
	$("#redeemcustomerpointTable").hide();
	 var table1 = $('#redeemable_item_table').DataTable();
	 table1.destroy();
	 getProducts()
	 cartList = [];
	 cartCount = 0;
}

function getProducts(){
	var t = $("#xpoint").text();
	$("#customerRemainingPoint").text()
	$("#cartcount").text(0);
	 var productRequestData = {"availabilityStatus":"T"}
    $.ajax({
		 type: "POST",
		  url: ctx+"/get-all-product",
		  data: JSON.stringify(productRequestData),
		  dataType: "json",
		  contentType : "application/json",
		  success: function(response){
			var result = JSON.parse(response.data)
		    if(response.responseCode == "00"){
		    	var data = [];
		    	var i =0;
		    	for(var i in result) {
                     m = parseInt(i)+1;
		    	     var item = result[i];
                      data.push({
                    	"sno" : m,
		    	        "product_NAME" : item.product_NAME,
		    	        "product_POINT_VALUE"  : item.product_POINT_VALUE,
		    	        "product_CATEGORY"       : item.product_CATEGORY,
		    	        "product_ID"       : item.product_ID,
		    	    });
		    	}
		    	
		    	
		    	$('#redeemable_item_table').DataTable( {
		    	
	          "data": data,
	          "columns": [
	        { "data": "sno" },
	        { "data": "product_NAME" },
	        { "data": "product_POINT_VALUE" },
	        { "data": "sno" }, 
	        { "data": "sno" }, 
	        { "data": "sno"},
	      ],
	      "columnDefs": [
	    	  {
	  	      	"render": function (data, type, row) {
	  	      		return '<input type="number" value="0" id="w' + data + '" style="width:50px;color:black"/>' ;
	  	      	   },
	  	      	"targets": 3
	  	      	},
	      	{
	      	"render": function (data, type, row) {
	      		return '<input type="number" disabled="disabled" value="0" id="t' + data + '" style="width:50px;color:black"/><input type="hidden" disabled="disabled" value="0" id="j' + data + '" style="width:30px;color:black"/>' ;
	      	   },
	      	"targets": 4
	      	},
	      	{
		     "render": function (data, type, row) {
		      		return '<button type="button" id="b' + data + '" onclick="addProduct(\'' + data + '\')" class="" style="background:#337ab7"><i class="fa fa-check"></i></button>&nbsp;<button type="button" style="background:#d9534f" onclick="removProduct(\'' + data + '\')" id="c' + data + '" class="" ><i class="fa fa-remove"></i></button>' ;
		      	},
		      	"targets": 5
		      	}
	      	]
	  } );
	 }
			   else{
				     swal(
						  'Failed Operation',
						   response.responseMessage,
						  'error'
					   ) 
			      }
			 },
		  error: function() {
			        swal(
						  'OOPS an error occured!',
						  'Kindly contact an administrator',
						  'error'
					   )
					   }
	 });
	}