var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));

$('#xContactCenterApprovalForm').on('hidden.bs.modal', function () {
   customerApprovalReset();
	});

$('#xRmPendingReferrredCustomerModal').on('hidden.bs.modal', function () {
	var table = $('#xRM_pending_referred_customer_table').DataTable();
	table.destroy();
	});

$('#xGhPendingReferrredCustomerModal').on('hidden.bs.modal', function () {
	var table = $('#xGH_pending_referred_customer_table').DataTable();
	table.destroy();
});

$('#xContactCenterReferrredCustomerModal').on('hidden.bs.modal', function () {
	var table = $('#pending_referred_customer_table').DataTable();
	table.destroy();
});

$('#xApprovedCustomer').on('hidden.bs.modal', function () {
	var table = $('#approved_customer_table').DataTable();
	table.destroy();
	});

$('#xDeclinedCustomer').on('hidden.bs.modal', function () {
	var table = $('#declined_customer_table').DataTable();
	table.destroy();
	});

$('#xAllCustomer').on('hidden.bs.modal', function () {
	var table = $('#all_customer_table').DataTable();
	table.destroy();
	});

$('#xContactCenterForm').on('hidden.bs.modal', function () {
	customerApprovalReset();
	});

$('#xAllHssaAndAdminModal').on('hidden.bs.modal', function () {
var table = $('#xAllHssaAndAdminTable').DataTable()
table.destroy();
});
function submitGHCustomerApproval(){
	     var ghApprovalData = buildJsonFromFormData("#ghApprovalForm"); 
	         $.ajax({
					  type: "POST",
					  url: ctx+"/saveGroupHeadRemark",
					  data: ghApprovalData,
					  dataType: "json",
					  contentType : "application/json",
					  beforeSend: function(){
						  $("#ghcustomerapprovalpreloader").show();
					    },
					  complete: function(){
						 $("#ghcustomerapprovalpreloader").hide();
					    },
					  success: function(response){
						  if(response.responseCode == "00"){
							  swal({
								  title: 'Success Operation',
								  type: 'success',
								  text: 'Referred Customer Request has been pushed to Relationship Manager!'
							  }); 
							  var sesX = $("#sesX").text();
							  
							  afterGHSubmitCustomerApproval(sesX)
							 // setTimeout(function(){window.location.href = '/home'}, 3000);
						    //----call reset function
						    //------Recall modal 
						  //  getCustomer('GH','O','pending',ghApprovalData.transId);
						    
						  }
						  
						   else{
							    /*swal(
									  'Failed Operation',
									   response.responseMessage,
									  'error'
								   ) */
						     }
						 },
					  error: function() {
						        swal(
									  'OOPS an error occured!',
									  'Kindly contact an administrator',
									  'error'
								   )
							}
				   });
}

function submitRMCustomerApproval(){
	var rmApprovalData = buildJsonFromFormData("#rmApprovalForm"); 
	         $.ajax({
					  type: "POST",
					  url: ctx+"/saveRMRemark",
					  data: rmApprovalData,
					  dataType: "json",
					  contentType : "application/json",
					  beforeSend: function(){
						  $("#rmcustomerapprovalpreloader").show();
					    },
					  complete: function(){
						 $("#rmcustomerapprovalpreloader").hide();
					    },
					  success: function(response){
						  if(response.responseCode == "00"){
							  swal({
								  title: 'Success Operation',
								  type: 'success',
								  text: 'Customer Referral has been succesfully approved'
							  }); 
							  var sesX = $("#sesX").text();
							   afterRMSubmitCustomerApproval(sesX);
						   }
						  
						   else{
							    /*swal(
									  'Failed Operation',
									   response.responseMessage,
									  'error'
								   )*/ 
						     }
						 },
					  error: function() {
						        swal(
									  'OOPS an error occured!',
									  'Kindly contact an administrator',
									  'error'
								   )
							}
				   });
}


function customerApprovalReset(){
	$('#status').get(0).selectedIndex = 0;
	$('#dateOfCall').val("");
	$('#timeOfCall').val("");
	$('#venueOfCall').val("");
	$('#cityOfCall').val("");
	$('#ghs').hide();
	$('#groupHead').get(0).selectedIndex = 0;
}

function GHCustomerApprovalReset(){
	$("#remark").val("");
}



//----
function afterSubmitCustomerApproval(){
	customerApprovalReset();
	
	var table = $('#pending_referred_customer_table').DataTable();
	table.destroy();
	
	$("#xContactCenterApprovalForm").modal('hide');
	getCustomer('CC','O','pending','');
	
	 swal({
		  title: 'Confirmation Message',
		  text: 'Would you like to treat another request?',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No'
		}).then((result) => {
		  if (result.value) {
		 
		  }
		  else{
			window.location.href= "/home";
		}
	}) 
}

//---
function afterGHSubmitCustomerApproval(staffid){
	GHCustomerApprovalReset()
	
	var table = $('#xGH_pending_referred_customer_table').DataTable();
	table.destroy();
	
	$("#xGHApprovalForm").modal('hide');
	
	getCustomer('GH','O','pending',staffid);
	
	swal({
		  title: 'Confirmation Message',
		  text: 'Would you like to treat another request?',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No'
		}).then((result) => {
		  if (result.value) {
		 
		  }
		  else{
			window.location.href= "/home";
		}
	})  
}



function RMCustomerApprovalReset(){
	$('#remark').val("");
	$('#accountNumberw').val("");
}
//---
function afterRMSubmitCustomerApproval(staffid){
	RMCustomerApprovalReset();
	
	var table = $('#xRM_pending_referred_customer_table').DataTable();
	table.destroy();
	
	$("#xRMApprovalForm").modal('hide');
	
	getCustomer('RM','O','pending',staffid);
	
	swal({
		  title: 'Confirmation Message',
		  text: 'Would you like to treat another request?',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No'
		}).then((result) => {
		  if (result.value) {
		 
		  }
		  else{
			window.location.href= "/home";
		}
	})  
}

function confirmRMApproval(){
	 swal({
	  title: 'Confirmation Message',
	  text: 'Are you sure you want to approve this request?',
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes',
	  cancelButtonText: 'No'
	}).then((result) => {
	  if (result.value) {
		  submitRMCustomerApproval()
	  }
	  else{
		  
	}
})
}


function confirmGHApproval(){
		 swal({
		  title: 'Confirmation Message',
		  text: 'Are you sure you want to approve this request?',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No'
		}).then((result) => {
		  if (result.value) {
			  submitGHCustomerApproval()
		  }
		  else{
			  
		}
})
}





function confirmContactCenterApproval(){
	//var referCustomerData = buildJsonFromFormData("#refersomeone"); 
	//var validationRsp  = validateCustomerReferralData(referCustomerData)
	//if(validationRsp.startsWith("successful")){
		 swal({
		  title: 'Confirmation Message',
		  text: 'Are you sure you want to approve this request?',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No'
		}).then((result) => {
		  if (result.value) {
			  submitCustomerApproval()
		  }
		  else{
			  
		}
})
	/*}else{
		  swal(
				  'Failed Validation Response',
				  validationRsp,
				  'error'
			   )
	}*/
}


function submitCustomerApproval(){
	var customerApprovalData = buildJsonFromFormData("#customerApprovalForm");
	    $.ajax({
					  type: "POST",
					  url: ctx+"/saveReferalConsent",
					  data: customerApprovalData,
					  dataType: "json",
					  contentType : "application/json",
					  beforeSend: function(){
						  $("#customerapprovalpreloader").show();
					    },
					  complete: function(){
						 $("#customerapprovalpreloader").hide();
					    },
					  success: function(response){
						  if(response.responseCode == "00"){
							  swal({
								  title: 'Success Operation',
								  type: 'success',
								  text: 'Referred Customer Request has been pushed to group head!'
								}); 
							  afterSubmitCustomerApproval();
						   }
						  else if(response.responseCode == "01"){
							  swal({
								  title: 'Success Operation',
								  type: 'success',
								  text: response.responseMessage
								}); 
							  afterSubmitCustomerApproval();  
						  }
						  
						   else{
							    /*swal(
									  'Failed Operation',
									   response.responseMessage,
									  'error'
								   )*/ 
						     }
						 },
					  error: function() {
						        swal(
									  'OOPS an error occured!',
									  'Kindly contact an administrator',
									  'error'
								   )
							}
				   });
}




function showGroupHead(){

	var decision_stat = $("#status").val();
	if(decision_stat == "Y"){
		$("#ghs").show(1000);
	}else{
		$("#ghs").hide(1000);
	}
}




function showRMCustomerDetails(transactionId){{
	  var request = {"transId":transactionId,"queryCode":"RM"};
	  $("#transId3").val(transactionId);
	   $.ajax({
			  type: "POST",
			  url: ctx+"/get-customer-details-by-txnid",
			  data:JSON.stringify(request),
			  dataType: "json",
			  contentType : "application/json",
			  success: function(response){
				  if(response.responseCode == "00"){
                      var data = JSON.parse(response.data)
					  $("#customerName4").val(data.firstname+" "+data.lastname);
				      $("#transId4").val(transactionId);	
				      $("#refcode4").val(data.refcode);
			    } else{
					   /* swal(
							  'Failed Operation',
							   response.responseMessage,
							  'error'
						   ) */
				     }
				 },
			  error: function() {
				        swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   ) }
		         });


	  
	  
	  
	  
	  
	  
	  $("#xRMApprovalForm").modal();
	}
}

function showGHCustomerDetails(transactionId){{
	  var request = {"transId":transactionId,"queryCode":"GH"};
		 $.ajax({
				  type: "POST",
				  url: ctx+"/get-customer-details-by-txnid",
				  data:JSON.stringify(request),
				  dataType: "json",
				  contentType : "application/json",
				  success: function(response){
					  if(response.responseCode == "00"){
                            var data = JSON.parse(response.data)
						  $("#customerName2").val(data.firstname+" "+data.lastname);
					     $("#transId2").val(transactionId);
					     
              var dataList = JSON.parse(response.dataList);
              var space = "---------";
             for (i in dataList ) {
           	  $('#rMy').append('<option value="' + dataList[i].emp_NUMBER+ '">' + dataList[i].full_NAME +space+ dataList[i].location+'</option>');
             }
             $('#rMy').selectize({
           	  plugins: ['remove_button']
           	  });	  
				 } else{
						   /* swal(
								  'Failed Operation',
								   response.responseMessage,
								  'error'
							   )*/ 
					     }
					 },
				  error: function() {
					        swal(
								  'OOPS an error occured!',
								  'Kindly contact an administrator',
								  'error'
							   ) }
			         });
	
	
		$("#xGHApprovalForm").modal();
	}
}









function showCustomerDetails(transactionId){
		var request = {"transId":transactionId,"queryCode":"CC"};
		$.ajax({
				  type: "POST",
				  url: ctx+"/get-customer-details-by-txnid",
				  data:JSON.stringify(request),
				  dataType: "json",
				  contentType : "application/json",
				  success: function(response){
					  if(response.responseCode == "00"){
                          var data = JSON.parse(response.data)
						  $("#customerName").val(data.firstname+" "+data.lastname);
					      $("#transId").val(transactionId);
                   var dataList = JSON.parse(response.dataList);
                   var space = "---------";
                  for (i in dataList ) {
                	  $('#groupHead').append('<option value="' + dataList[i].emp_NUMBER+ '">' + dataList[i].full_NAME +space+ dataList[i].location+'</option>');
                  }
                  $('#groupHead').selectize({
                	  plugins: ['remove_button']
                	  });	  
				 } else{
						   /* swal(
								  'Failed Operation',
								   response.responseMessage,
								  'error'
							   ) */
					     }
					 },
				  error: function() {
					        swal(
								  'OOPS an error occured!',
								  'Kindly contact an administrator',
								  'error'
							   ) }
			         });
	
	
	$("#xContactCenterApprovalForm").modal();
}

function getCustomer(dept,status,table,staffId){
	var request = {"queryCode":dept, "statusCode":status, "staffId":staffId};
	  if(dept == "CC" || dept == "RM" || dept == "GH"){
		  $.ajax({
			  type: "POST",
			  url: ctx+"/get-referral-by-deptcode-and-status",
			  data: JSON.stringify(request),
			  dataType: "json",
			  contentType : "application/json",
			  success: function(response){
				     var data = JSON.parse(response.data)
				  if(response.responseCode == "00"){
					  var counter = null;
			if(table == "pending"){	
					var tablename = null;
				
		    if(dept == "CC"){
					tablename = '#pending_referred_customer_table';
			 $(tablename).DataTable( {
			   "scrollCollapse": false,
		       "bScrollCollapse": true,
               "data": data,
               "columns": [
              { "data": "firstname" },
              { "data": "lastname" },
              { "data": "phone" },
              { "data": "refcode" },
              { "data": "location" },
              { "data": "reqstatus" },
              { "data": "datecreated" },
              { "data": "transid" }
            ],
            "columnDefs": [
            	{
            	"render": function ( data, type, row ) {
            	 return "<button class='btn btn-danger btn_xs' onclick=showCustomerDetails("+data+")><i style='color:#fff' class='fa fa-tasks'></i></button>";
            	},
            	"targets": 7
            	}
            	]
        } );
                   }

           else if(dept == "GH"){
					tablename = '#xGH_pending_referred_customer_table';
				$(tablename).DataTable( {
			   "scrollCollapse": false,
		       "bScrollCollapse": true,
               "data": data,
               "columns": [
              { "data": "firstname" },
              { "data": "lastname" },
              { "data": "phone" },
              { "data": "refcode" },
              { "data": "location" },
              { "data": "reqstatus" },
              { "data": "datecreated" },
              { "data": "transid" }
            ],
            "columnDefs": [
            	{
            	"render": function ( data, type, row ) {
            	 return "<button class='btn btn-danger btn_xs' onclick=showGHCustomerDetails("+data+")><i style='color:#fff' class='fa fa-tasks'></i></button>";
            	},
            	"targets": 7
            	}
            	]
               } );	
      	} else if(dept == "RM"){
					tablename = '#xRM_pending_referred_customer_table';
					
				$(tablename).DataTable( {
			   "scrollCollapse": false,
		       "bScrollCollapse": true,
               "data": data,
               "columns": [
              { "data": "firstname" },
              { "data": "lastname" },
              { "data": "phone" },
              { "data": "refcode" },
              { "data": "location" },
              { "data": "reqstatus" },
              { "data": "datecreated" },
              { "data": "transid" }
            ],
            "columnDefs": [
            	{
            	"render": function ( data, type, row ) {
            	 return "<button class='btn btn-danger btn_xs' onclick=showRMCustomerDetails("+data+")><i style='color:#fff' class='fa fa-tasks'></i></button>";
            	},
            	"targets": 7
            	}
            	]
               } );	
				}
      	
	    }
	 else if(table == "approved"){	  
			 $('#approved_customer_table').DataTable( {
		    	"scrollCollapse": false,
		    	"bScrollCollapse": true,
                "data": data,
                "columns": [
               { "data": "reqstatus" },
              { "data": "firstname" },
              { "data": "lastname" },
              { "data": "phone" },
              { "data": "refcode" },
              { "data": "location" },
              { "data": "reqstatus" },
              { "data": "datecreated" }     
            ],
            "columnDefs": [
            	{
            	"render": function ( data, type, row,meta ) {
            	   return meta.row + meta.settings._iDisplayStart + 1;;
            	},
            	"targets": 0
            	}
            	]
        } );	
 }
				 else if(table == "declined"){	  
			 $('#declined_customer_table').DataTable( {
		    	"scrollCollapse": false,
		    	"bScrollCollapse": true,
                "data": data,
                "columns": [
              { "data": "reqstatus" },
              { "data": "firstname" },
              { "data": "lastname" },
              { "data": "phone" },
              { "data": "refcode" },
              { "data": "location" },
              { "data": "reqstatus" },
              { "data": "datecreated" }     
            ],
            "columnDefs": [
            	{
            	"render": function ( data, type, row,meta ) {
            	   return meta.row + meta.settings._iDisplayStart + 1;;
            	},
            	"targets": 0
            	}
            	]
        } );	
            }
				     }
				   else{
					     /*swal(
							  'Failed Operation',
							   response.responseMessage,
							  'error'
						   ) */
				      }
				 },
			  error: function() {
				        swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )
						   }
		   });
		 
	  } else{
		  getRequestInfoForAdminAndHssa(table); 
	  }
}






function getAllReferredCustomer(){
	var tab = $('#all_customer_table').DataTable();
	tab.destroy()
	$.ajax({
		  type: "GET",
		  url: ctx+"/get-all-refered-customer",
		  dataType: "json",
		  contentType : "application/json",
		  success: function(response){
			var data = JSON.parse(response.data)
		    if(response.responseCode == "00"){
		    	 
		      $('#all_customer_table').DataTable( {
		    	"scrollCollapse": false,
		    	"bScrollCollapse": true,
                "data": data,
                "columns": [
               { "data": "reqstatus" },
              { "data": "firstname" },
              { "data": "lastname" },
              { "data": "phone" },
              { "data": "refcode" },
              { "data": "location" },
              { "data": "reqstatus" },
              { "data": "datecreated" }     
            ],
            "columnDefs": [
            	{
            	"render": function ( data, type, row,meta ) {
            	   return meta.row + meta.settings._iDisplayStart + 1;;
            	},
            	"targets": 0
            	}
            	]
        } );

		      $("#xAllCustomer").modal()
			  }
			   else{
				     /*swal(
						  'Failed Operation',
						   response.responseMessage,
						  'error'
					   ) */
			      }
			 },
		  error: function() {
			        swal(
						  'OOPS an error occured!',
						  'Kindly contact an administrator',
						  'error'
					   )
					   }
	   });
	}












function getRequestInfoForAdminAndHssa(reportType){
	   var request = {"reportType":reportType};
	   $.ajax({
			  type: "POST",
			  url:ctx+ "/get-request-info-for-admin-and-hssa",
			  data: JSON.stringify(request),
			  dataType: "json",
			  contentType : "application/json",
			  success: function(response){
			var data = JSON.parse(response.data)
		    if(response.responseCode == "00"){
		    	$('#xReportTitle').text(reportType.toUpperCase()+" CUSTOMER TABLE")
		      var table = $('#xAllHssaAndAdminTable').DataTable()
		      table.destroy();
		    	$('#xAllHssaAndAdminTable').DataTable( {
		    		"scrollCollapse": false,
			    	"bScrollCollapse": true,
                "data": data,
                "columns": [
                	{ "data": "reqstatus" },
                    { "data": "firstname" },
                    { "data": "lastname" },
                    { "data": "phone" },
                    { "data": "refcode" },
                    { "data": "location" },
                    { "data": "departmentcode" },
                    { "data": "treatedby" },
                    { "data": "reqstatus" },
                    { "data": "datecreated" }    
            ],
            "columnDefs": [
            	{
            	"render": function ( data, type, row,meta ) {
            	   return meta.row + meta.settings._iDisplayStart + 1;;
            	},
            	"targets": 0
            	}
            	]
        } );
			  }
			   else{
				    /* swal(
						  'Failed Operation',
						   response.responseMessage,
						  'error'
					   ) */
			      }
			 },
		  error: function() {
			        swal(
						  'OOPS an error occured!',
						  'Kindly contact an administrator',
						  'error'
					   )
					   }
	   });
	}







function edit(transid){
	alert(transid);
}

