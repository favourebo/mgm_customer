var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));


function buildJsonFromFormData(selector){
	 var ary = $(selector).serializeArray();
	 var obj = {};
	 for (var a = 0; a < ary.length; a++) obj[ary[a].name] = ary[a].value;
	 return JSON.stringify(obj);
}

function doCustomerRegistrationReset(){
	    $("#validate").show();
	    $(".g-recaptcha").show();
		$("#signup").hide();
		$("#mobilephone").hide();
		$("#accountname").hide();
		$("#emailaddress").hide();
		$("#notification").hide();
		$("#label").hide();
		$("#reset").hide();
		$("#mobilephone").val("");
	    $("#accountname").val("");
		$("#emailaddress").val("");	
		$("#accountnumber").val("");	
		grecaptcha.reset();
}

function registerCustomer(){
	 var accNum = $('#accountnumber').val();
	 var request = {"accountNumber":accNum}
	  console.log(request);
	  $.ajax({
		  type: "POST",
		  url: ctx+"/register-customer",
		  data: JSON.stringify(request),
		  dataType: "json",
		  contentType:"application/json",
		  beforeSend: function(){
			    $("#registerclientpreloader").show();
			    $("#mobilephone").hide();
				$("#accountname").hide();
				$("#emailaddress").hide();
				$("#notification").hide();
		    },
		  complete: function(){
			 $("#registerclientpreloader").hide();
			 $("#mobilephone").show();
				$("#accountname").show();
				$("#emailaddress").show();
				$("#notification").show();
		    },
		  success: function(response){
			  if(response.responseCode == "00"){
				  $("#validate").show();
				    $(".g-recaptcha").show();
					$("#signup").hide();
					$("#mobilephone").hide();
					$("#accountname").hide();
					$("#emailaddress").hide();
					$("#notification").hide();
					$("#label").hide();
					$("#reset").hide();
					$("#mobilephone").val("");
				    $("#accountname").val("");
					$("#emailaddress").val("");	
					$("#accountnumber").val("");	
					grecaptcha.reset();
				  swal({
					  title: 'Customer Registration was successful',
					  type: 'success',
					  text: 'Referral code has been sent to customers registered mobile'
					}); 
				  
				   }
			  else{
				    swal(
						  'Failed Operation',
						  response.responseMessage,
						  'error'
					   ) 
			     }
			 },
		  error: function(xhr, ajaxOptions, thrownError) {
			  alert(xhr.responseText);
			        swal(
						  'OOPS an error occured!',
						  'Kindly contact an administrator',
						  'error'
					   )
              }
	   });   
}













$(document).ready(function() {
	var contactForm = $("#registerCustomerForm");
   contactForm.on("submit", function(e) {
   e.preventDefault();
 
  var registerCustomerFormData = buildJsonFromFormData("#registerCustomerForm");
  xregisterCustomerFormData =  JSON.parse(registerCustomerFormData);
  xregisterCustomerFormData.captcha = grecaptcha.getResponse();
  
    console.log(xregisterCustomerFormData);
    
    $.ajax({
		  type: "POST",
		  url: ctx+"/validate-account-number",
		  data: JSON.stringify(xregisterCustomerFormData),
		  dataType: "json",
		  contentType : "application/json",
		  beforeSend: function(){
			  $("#registerclientpreloader").show();
		    },
		  complete: function(){
			  $("#registerclientpreloader").hide();
		   },
		  success: function(response){
			  console.log(response)
			   var data = JSON.parse(response.data)
			  if(response.responseCode == "00"){
				    $("#validate").hide();
				    $(".g-recaptcha").hide();
					$("#signup").show(1000);
					$("#mobilephone").show(1000);
					$("#accountname").show(1000);
					$("#emailaddress").show(1000);
					$("#notification").show(1000);
					$("#label").show(1000);
					$("#reset").show(1000);
					if(data.mobile_NUMBER == null || data.mobile_NUMBER == ""){
						$("#mobilephone").val("PHONE: N/A");	
					}else{
						$("#mobilephone").val("PHONE: "+data.mobile_NUMBER);	
					}
				    $("#accountname").val("NAME: "+data.ac_DESC);
					
				    if(data.e_MAIL == null || data.e_MAIL == ""){
						$("#emailaddress").val("EMAIL: N/A");	
					 }else{
						$("#emailaddress").val("EMAIL: "+data.e_MAIL);	
					}
				   }
			  else{
				    swal(
						  'Failed Operation',
						  response.responseMessage,
						  'error'
					   ) 
			  }
			  grecaptcha.reset();
			 },
		  error: function() {
			        swal(
						  'OOPS an error occured!',
						  'Kindly contact an administrator',
						  'error'
					   )
			  // setTimeout(function(){window.location.href = '/offline_envelope_deposit'  }, 2000);
            }
	   });
    
    
    
    
    
    
    
    
    
    
  });

});



