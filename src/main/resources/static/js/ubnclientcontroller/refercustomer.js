var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));


function buildJsonFromFormData(selector){
	 var ary = $(selector).serializeArray();
	 var obj = {};
	 for (var a = 0; a < ary.length; a++) obj[ary[a].name] = ary[a].value;
	 return JSON.stringify(obj);
}

//This resets the registration form
function doReferralReset(){
	$("#firstName").val("");
	$("#surname").val("");
	$("#mobilePhone").val("");
	$("#email").val("");
	$("#address").val("");
	$("#referralCode").val("");
}



//populate location drop down
$(document).ready(function() {
	populateLocation();	
});



function populateLocation(){
	$('#address').empty();
	 $.ajax({
			  type: "GET",
			  url: ctx+"/get-all-location",
			  contentType : "application/json",
			  success: function(response){
				  if(response.responseCode == "00"){
                     var data = JSON.parse(response.data)
         for (i in data ) {
        	 var xval = data[i].area_ID+"|"+data[i].area_NAME;
    	   $('#address').append('<option value="'+xval+'">' + data[i].area_NAME+'</option>');
       }
                     var options = $("#address option");                            
                     options.detach().sort(function(a,b) {               
                         var at = $(a).text();
                         var bt = $(b).text();         
                         return (at > bt)?1:((at < bt)?-1:0);            
                     });
                     options.appendTo("#address");
			 } else{
					    swal(
							  'Failed Operation',
							   response.responseMessage,
							  'error'
						   ) 
				     }
				 },
			  error: function() {
				        swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   ) }
		         });
}



function confirmReferCustomer(){
	var referCustomerData = buildJsonFromFormData("#refersomeone"); 
	var xData = JSON.parse(referCustomerData);
	var validationRsp  = validateCustomerReferralData(referCustomerData)
	
	if(xData.relationship == "d"){
		  swal(
				  'Failed Operation',
				  'Kindly select a relationship!',
				  'error'
            )
	}
	else if(xData.relationship == "b" || xData.relationship == "s" || xData.relationship == "f" || xData.relationship == "m"){
					  swal(
							  'Failed Operation',
							  'You cannot refer your brother, sister, father and mother!',
							  'error'
	)
	 }
	 else if(validationRsp.startsWith("successful")){
		 swal({
		  title: 'Confirmation Message',
		  text: 'Are you sure you want to refer customer?',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No'
		}).then((result) => {
		  if (result.value) {
			 referCustomer();
		  }
		  else{
			  doReferralReset();
		}
})
	}else{
		  swal(
				   'Failed Validation Response',
				   validationRsp,
				   'error'
			   )
	}
}	


function referCustomer(){
	var referCustomerData = buildJsonFromFormData("#refersomeone"); 
	$.ajax({
			  type: "POST",
			  url: ctx+"/refer-customer",
			  data: referCustomerData,
			  dataType: "json",
			  contentType : "application/json",
			  beforeSend: function(){
				  $("#refercustomerpreloader").show();
			    },
			  complete: function(){
				 $("#refercustomerpreloader").hide();
			    },
			  success: function(response){
				  if(response.responseCode == "00"){
					  doReferralReset();
					  swal({
						  title: 'Customer Referal was successful',
						  type: 'success',
						  text: 'Customer details has been sent to contact center for follow up!'
						}); 
					 setTimeout(function(){window.location.href = ctx+'/home'}, 3000);
				    }
				   else{
					    swal(
							  'Failed Operation',
							   response.responseMessage,
							  'error'
						   ) 
				     }
				 },
			  error: function() {
				        swal(
							  'OOPS an error occured!',
							  'Kindly contact an administrator',
							  'error'
						   )
				  // setTimeout(function(){window.location.href = '/offline_envelope_deposit'  }, 2000);
	              }
		   });
	
}




$('#xDoReferral').on('hidden.bs.modal', function () {
	doReferralReset();
	});