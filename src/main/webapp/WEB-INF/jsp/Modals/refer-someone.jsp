<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ubn.Utility.Utils" %>
<!-- DO REFERRAL MODAL -->
<div id="xDoReferral" class="modal fade" role="dialog">
  <div class="modal-dialog modal-xs" style="border-radius:20px 20px 20px 20px">
<form id="refersomeone">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color:#00AEEF;color:white">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<img src="images/stallionlogo.png" height="30px" style="float:right">
        <h4 class="modal-title" >REFER SOMEONE</h4>
      </div>
      <div class="modal-body" style="background:url('images/unionbankbackground.png');">
	   

    	<div class="input-group">
        <span class="input-group-addon" style="background:#00AEEF; color:#fff">Surname</span>
        <input id="surname" type="text" class="form-control" name="surname" placeholder="">
       </div>
      
	  <br />
	    <div class="input-group">
        <span class="input-group-addon" style="background:#00AEEF; color:#fff">First Name</span>
        <input id="firstName" type="text" class="form-control" name="firstName" placeholder="">
       </div>
	    
	    <br />
	    <div class="input-group">
        <span class="input-group-addon" style="background:#00AEEF; color:#fff">Relationship</span>
        <select id="relationship" class="form-control" name="relationship" >
          <option value="d"></option>
          <option value="b">BROTHER</option>
          <option value = "s">SISTER</option>
          <option value="f">FATHER</option>
          <option value="m">MOTHER</option>
          <option value="o">OTHERS</option>
        </select>
       </div>
	   
	    <br />
	    <div class="input-group">
        <span class="input-group-addon" style="background:#00AEEF; color:#fff">Mobile Phone</span>
        <input id="mobilePhone" type="text" class="form-control" name="mobilePhone" placeholder="">
       </div>
	   
	   <br />
	    <div class="input-group">
        <span class="input-group-addon" style="background:#00AEEF; color:#fff">Location</span>
        <select id="address" class="form-control" name="address" >
        </select>
       </div>
	   
	   <br />
	    <div class="input-group">
        <span class="input-group-addon" style="background:#00AEEF; color:#fff">Email</span>
        <input id="email" type="email" class="form-control" name="email" placeholder="">
       </div>
	   
	   <br />
	    <div class="input-group">
        <span class="input-group-addon" style="background:#00AEEF; color:#fff"> My Referral Code</span>
        <input id="xxxreferralCode" type="text" class="form-control" value="<%= session.getAttribute("empNumber") %>" disabled="disabled" placeholder="">
        <input id="referralCode" type="hidden" class="form-control" name="referralCode"  value='<%= Utils.encryptString((String) session.getAttribute("empNumber")) %>' placeholder="">
       </div>
	  
	   <div  id="refercustomerpreloader" style="display:none">
         <center><img src="images/regcustomergif.gif"/></center>
       </div>
      
       <div class="modal-footer">
       <div class="col-md-12">
       <div class="col-md-5"> 
        <button type="button" style="width:100%" class="btn btn-primary"  id="submitRegisteredCustomerbtn" onclick="confirmReferCustomer()">Refer</button>
       </div>  
         <div class="col-md-2"> </div> 
       <div class="col-md-5"> 
            <button type="button" style="width:100%;background:#d9534f" class="btn btn-danger"   onclick="doReset()">Reset</button>
            </div>  
     </div>
      </div>
       </div>
    </div>
</form>
  </div>
</div>
<!--DO REFERRAL MODAL END-->