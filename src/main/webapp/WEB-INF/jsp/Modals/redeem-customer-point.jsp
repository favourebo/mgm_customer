<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- REFERRAL VALIDATION MODAL  -->
<div id="xRedeemCustomerPoint" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="border-radius:20px 20px 20px 20px;width:90%">

    <!-- Modal content-->
     <div class="modal-content">
         <div class="modal-header" style="background-color:#00AEEF;color:white">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
		 <img src="images/stallionlogo.png" height="30px" style="float:right">
         <h4 class="modal-title" >REDEEM CUSTOMER POINT</h4>
       </div>
      
        <div class="modal-body" style="background:url('images/unionbankbackground.png');">
       
        
          <div class="work-progres" id="redeemcustomerpointTable" style="color:white; ">
              <div class="row">            
                         
            <div class="col-md-5"> 
             <div class="panel panel-default"> 
             <div class="panel-heading">Customer Details <i style="float:right">Point Left: <span class="badge" id="customerRemainingPoint" style="background:green">0</span></i> </div>
            <div class="panel-body">        
               <div class="table-responsive">
                                <table class="table table-bordered"  style="font-size:12px;background:#00000042;">
             <tbody>
        	 	<tr>
        		<td>Customer Name</td>
        		<td id="xname"></td>
        	</tr>
        	
        	<tr>
        		<td>Segment Description</td>
        		<td id="xsegmentDesc"></td>
        	</tr>
        	
        	<tr>
        		<td>Redeemable Point</td>
        		<td id="xpoint">0</td>
        	</tr>
          
            </tbody>

        </table>
          </div>
          </div>
          </div>
           </div>
       <div class="col-md-7">
       <div class="panel panel-default"> 
             <div class="panel-heading">Redeemable Item (s)    <a href="#" data-toggle="modal" data-target="#xCartModal" onclick="showCartInformation()" style="float:right"><i class="fa fa-shopping-cart"   style="height:30px"></i><span class="badge" id="cartcount" style="background:green">0</span></a> </div>
            <div class="panel-body">
        <div class="table-responsive">
        <table class="table table-bordered"  style="font-size:12px; background:#00000042; width:100%" id="redeemable_item_table">
            <thead>
        	  <tr>
        	     <th>Sno</th>
        		<th>Product Name</th>
        		<th>Point Per Product</th>
        		<th>Quantity</th>
        		<th>Total Point(s)</th>
        		<th>Action</th>
        		</tr>
            </thead>
        	

        </table>
         </div> 
            </div>    
             </div>
             </div>
             </div>
 </div>
        <div  id="redeemcustomerpointpreloader" style="display:none">
         <center><img src="images/regcustomergif.gif"/></center>
       </div>
      
      
      
      
    
     </form>
     
    </div >

    
  </div>
</div>
</div>
<!--REFERRAL VALIDATION MODAL END-->