<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ubn.Utility.Utils" %>

<div id="xReferralListModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width:80%">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color:#00AEEF;color:white">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<img src="images/stallionlogo.png" height="30px" style="float:right">
        <h4 class="modal-title" >MY REFERRAL HISTORY</h4>
      </div>
       <div class="modal-body" style="background:url('images/unionbankbackground.png');color:white">
					<div class="work-progres"
						style="color: white;">
						<div class="table-responsive">
							<table class="table table-bordered" style="font-size:12px;width:100%" id="xxreferral_list_table">
								<thead>
                                 <tr>
                                 		<th>FIRST NAME</th>
									<th>LAST NAME</th>
									<th>PHONE</th>
									<th>LOCATION</th>
									<th>EMAIL</th>
									<th>STATUS</th>
									<th>DATE CREATED</th>
								</tr>
							</thead>
							<caption><p id="statCode"></p></caption>
							</table>
						</div>
					</div>
				</div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
    </div>

  </div>
</div>

