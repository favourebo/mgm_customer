<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ubn.Utility.Utils" %>

<!DOCTYPE HTML>
<html>
<head>
<title>MGM</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<link href="${pageContext.request.contextPath}/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css" media="all"/>
<link href="${pageContext.request.contextPath}/css/client.css" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="${pageContext.request.contextPath}/js/ubnclientscript/jquery-2.1.1.min.js"></script> 
<!--icons-css-->
<link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet"> 
<!--Google Fonts-->
<link href='https://fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<style type="text/css">
* {box-sizing: border-box;}

/* Style the input container */
.input-container {
  display: flex;
  width: 100%;
  margin-bottom: 15px;
}

/* Style the form icons */
.icon {
  padding: 10px;
  background: dodgerblue;
  color: white;
  min-width: 50px;
  text-align: center;
}

/* Style the input fields */
.input-field {
  width: 100%;
  padding: 10px;
  outline: none;
}

.input-field:focus {
  border: 2px solid dodgerblue;
}

/* Set a style for the submit button */
.btn {
  background-color: dodgerblue;
  color: white;
  padding: 15px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.btn:hover {
  opacity: 1;
}
</style>
<!--static chart-->
<script src="${pageContext.request.contextPath}/js/ubnclientscript/Chart.min.js"></script>
<!--//charts-->
<!-- geo chart -->
    <script src="${pageContext.request.contextPath}/js/ubnclientscript/modernizr.min.js" type="text/javascript"></script>
    <script>window.modernizr || document.write('<script src="${pageContext.request.contextPath}/js/ubnclientscript/modernizr.custom.min.js"><\/script>')</script>
    <!--<script src="lib/html5shiv/html5shiv.js"></script>-->
     <!-- Chartinator  -->
    <script src="${pageContext.request.contextPath}/js/ubnclientscript/chartinator.js" ></script>
    <script type="text/javascript">
        jQuery(function ($) {

            var chart3 = $('#geoChart').chartinator({
                tableSel: '.geoChart',

                columns: [{role: 'tooltip', type: 'string'}],
         
                colIndexes: [2],
             
                rows: [
                    ['China - 2015'],
                    ['Colombia - 2015'],
                    ['France - 2015'],
                    ['Italy - 2015'],
                    ['Japan - 2015'],
                    ['Kazakhstan - 2015'],
                    ['Mexico - 2015'],
                    ['Poland - 2015'],
                    ['Russia - 2015'],
                    ['Spain - 2015'],
                    ['Tanzania - 2015'],
                    ['Turkey - 2015']],
              
                ignoreCol: [2],
              
                chartType: 'GeoChart',
              
                chartAspectRatio: 1.5,
             
                chartZoom: 1.75,
             
                chartOffset: [-12,0],
             
                chartOptions: {
                  
                    width: null,
                 
                    backgroundColor: '#fff',
                 
                    datalessRegionColor: '#F5F5F5',
               
                    region: 'world',
                  
                    resolution: 'countries',
                 
                    legend: 'none',

                    colorAxis: {
                       
                        colors: ['#679CCA', '#337AB7']
                    },
                    tooltip: {
                     
                        trigger: 'focus',

                        isHtml: true
                    }
                }

               
            });                       
        });
    </script>
<!--geo chart-->

<!--skycons-icons-->
<script src="${pageContext.request.contextPath}/js/ubnclientscript/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body onload="initializeApp('<%= Utils.encryptString((String) session.getAttribute("empNumber"))%>')">	
<div class="page-container">	
   <div class="left-content">
	   <div class="mother-grid-inner">
            <!--header start here-->
				<div class="header-main fixed">
					<div class="header-left">
							<div class="logo-name">
									 <a href="${pageContext.request.contextPath}/"> <h1 style="color:#00AEEF">MGM</h1> 
									<!--img id="logo" src="images/logo2.png"  height="30px" alt="Logo"/-->
								  </a> 								
							</div>
							<!--search-box-->
								<!--div class="search-box">
									<form>
										<input type="text" placeholder="Search..." required="">	
										<input type="submit" value="">					
									</form>
								</div--><!--//end-search-box-->
							<div class="clearfix"> </div>
						 </div>
						 <div class="header-right">
							<div class="profile_details_left"><!--notifications of menu start -->
							<img src="${pageContext.request.contextPath}/images/logo2.png" height="60px" width="80px" style="float:right"/>
							</div>
							<!--notification menu end -->
							<div class="profile_details">		
								<ul>
									<li class="dropdown profile_details_drop">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<div class="profile_img">	
												<span class="prfil-img"><img src="${pageContext.request.contextPath}/images/user.png" height="50px" alt=""> </span> 
												<div class="user-name">
													<p style="font-size:14px"><%= (String) session.getAttribute("fullName")%></p>
													<span><b><i><%= (String) session.getAttribute("channelCode") %></i></b></span>
												</div>
												<i class="fa fa-angle-down lnr"></i>
												<i class="fa fa-angle-up lnr"></i>
												<div class="clearfix"></div>	
											</div>	
										</a>
										<ul class="dropdown-menu drp-mnu">
											<!-- li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li--> 
											<li> <a href="${pageContext.request.contextPath}/logout"><i class="fa fa-sign-out"></i> Logout</a> </li>
										</ul>
									</li>
								</ul>
							</div>
							<div class="clearfix"> </div>				
						</div>
				     <div class="clearfix"> </div>	
				</div>
<!--heder end here-->
<!-- script-for sticky-nav -->
		<!--script>
		$(document).ready(function() {
			 var navoffeset=$(".header-main").offset().top;
			 $(window).scroll(function(){
				var scrollpos=$(window).scrollTop(); 
				if(scrollpos >=navoffeset){
					$(".header-main").addClass("fixed");
				}else{
					$(".header-main").removeClass("fixed");
				}
			 });
			 
		});
		</script -->
		<!-- /script-for sticky-nav -->
<!--inner block start here-->
<div class="inner-block">
<!--market updates updates-->
	 <div class="market-updates">
			<div class="col-md-4 market-update-gd">
				<a href="#" data-toggle="modal" data-target="#xReferralListModal" onclick="getReferralHistory('<%=Utils.encryptString((String) session.getAttribute("empNumber"))%>','N')">
	             <div class="market-update-block clr-block-1">
					<div class="col-md-8 market-update-left" style="text-shadow:1px 1px 1px black">
						<h3>0</h3>
						<h4>My Referral Points</h4>
						<p>1 point --> NGN 100</p>
					</div>
					<div class="col-md-4 market-update-right">
						<i class="fa fa-fax" style="color:#68ae00"> </i>
					</div>
				  <div class="clearfix"> </div>
				</div>
				</a>
			</div>
			<div class="col-md-4 market-update-gd">
				<a href="#" data-toggle="modal" data-target="#xReferralListModal" onclick="getReferralHistory('<%=Utils.encryptString((String) session.getAttribute("empNumber"))%>','ALL')">
				<div class="market-update-block clr-block-2">
				 <div class="col-md-8 market-update-left" style="text-shadow:1px 1px 1px black">
					<h3 id="xSize">0</h3>
					<h4>Total Referral (s)</h4>
					<p>1 referral --> 1 PT</p>
				  </div>
					<div class="col-md-4 market-update-right">
						<i class="fa fa-users" style="color:red"> </i>
					</div>
				  <div class="clearfix"> </div>
				</div>
				</a>
			</div>
			<div class="col-md-4 market-update-gd">
				<div class="market-update-block clr-block-3" style="text-shadow:1px 1px 1px black">
					<div class="col-md-8 market-update-left">
					<h3 class="blink"><%=(String) session.getAttribute("empNumber") %></h3>
						<h4>My Referral Code</h4>
						<p>Keep Safe</p>
					</div>
					<div class="col-md-4 market-update-right">
						<i class="fa fa-user" style="color:#337AB7"> </i>
					</div>
				  <div class="clearfix"> </div>
				</div>
			</div>
		   <div class="clearfix"> </div>
		</div>
<!--market updates end here-->
<!--mainpage chit-chating-->
<div class="chit-chat-layer1">
	 <div class="col-md-6">
        <div class="panel panel-success">
	 	<div class="panel-heading">BAR CHART ANALYTICS</div>
	 	<div class="panel-body">
    	<div class="css_bar_graph">
			
			<!-- y_axis labels -->
			<ul class="y_axis">
				<li>100%</li><li></li><li></li><li></li><li></li><li>50%</li><li></li><li></li><li></li><li></li><li>0%</li>
			</ul>
			
			<!-- x_axis labels -->
			<ul class="x_axis">
				<li>Request</li><li>Pending</li><li>Approved</li><li>Declined</li>
			</ul>
			
			<!-- graph -->
			<div class="graph">
				<!-- grid -->
				<ul class="grid">
					<li><!-- 100 --></li>			
					<li><!-- 90 --></li>				
					<li><!-- 80 --></li>				
					<li><!-- 70 --></li>				
					<li><!-- 60 --></li>
					<li><!-- 50 --></li>
					<li><!-- 40 --></li>
					<li><!-- 30 --></li>
					<li><!-- 20 --></li>
					<li><!-- 10 --></li>
					<li class="bottom"><!-- 0 --></li>
				</ul>
				
				<!-- bars -->
				<!-- 250px = 100% -->
				<ul>
					<li class="bar nr_1 purple" id="x_per_request" style="height: 250px; background:#00AEEF"><div class="top"></div><div class="bottom"></div><span id="total_request">100%</span></li>
					<li class="bar nr_2 orange" id="x_per_pending" style="background:#FF8C00"><div class="top"></div><div class="bottom"></div><span id="pending_barchart">0%</span></li>
					<li class="bar nr_3 green" id="x_per_approved" style="background:green"><div class="top"></div><div class="bottom" ></div><span id="approved_barchart">0%</span></li>
					<li class="bar nr_4 red" id="x_per_declined" style="background:red"><div class="top"></div><div class="bottom"></div><span id="declined_barchart">0%</span></li>
				</ul>	
			</div>
		</div>
		</div>
</div>      
     </div>
  
   <div class="col-md-6">
	 <div class="panel panel-success">
	 	<div class="panel-heading">PIE CHART ANALYTICS</div>
	 	<div class="panel-body">
	<canvas id="canvas" width="610" height="400"  >
    </canvas>
</div>
</div>
</div>
	
	
	
     <div class="clearfix"> </div>
</div>






<!--climate start here-->

<!--climate end here-->
</div>
<!--inner block end here-->
<!--copy rights start here-->
<div class="copyrights" style="background:url('images/unionbankbackground.png');">
	 <p>� 2018 MemberGetMember. All Rights Reserved | Design by  UBN IT Devops </p>
</div>	
<!--COPY rights end here-->
</div>


</div>
<!--slider menu-->
    <div class="sidebar-menu">
		  	<div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span> 
			      <!--<img id="logo" src="" alt="Logo"/>--> 
			  </a> </div>		  
		    <div class="menu">
		      <ul id="menu" >
		        <li id="menu-home" title="Dashboard"><a href="${pageContext.request.contextPath}/home"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
				
				 <li><a href="#" title="Do Referral" data-toggle="modal" data-target="#xDoReferral"><i class="fa fa-users"></i><span>Refer Someone</span><span class="fa fa-angle-right" style="float: right"></span></a>
		        </li>
		       
			   <li><a href="#" title="Redeem Points" data-toggle="modal" data-target="#xRedeemCustomerPoint" onclick="validateRefsCode('<%=Utils.encryptString((String) session.getAttribute("empNumber"))%>')"><i class="fa fa-shopping-cart"></i><span>Redeem Points</span><span class="fa fa-angle-right" style="float: right"></span></a></li>
			   
			  
			  
			  <li><a href="#" title="Referral History" data-toggle="modal" data-target="#xReferralListModal" onclick="getReferralHistory('<%=Utils.encryptString((String) session.getAttribute("empNumber"))%>','ALL')"><i class="fa fa-bar-chart"></i><span>Referral History</span><span class="fa fa-angle-right" style="float: right"></span></a></li>
		        <li id="menu-home" title="log-out"><a href="${pageContext.request.contextPath}/logout"><i
				class="fa fa-sign-out"></i><span>Log Out</span></a></li>
		       </ul>
		    </div>
	 </div>
	<div class="clearfix"> </div>
</div>

<jsp:include page="Modals/refer-someone.jsp" />
<jsp:include page="Modals/redeem-customer-point.jsp" />
<jsp:include page="Modals/referral-list.jsp" />



<script>
var toggle = true;
            
$(".sidebar-icon").click(function() {                
  if (toggle)
  {
    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
    $("#menu span").css({"position":"absolute"});
  }
  else
  {
    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
    setTimeout(function() {
      $("#menu span").css({"position":"relative"});
    }, 400);
  }               
                toggle = !toggle;
            });
</script>
<!--scrolling js-->
<script src="${pageContext.request.contextPath}/js/ubnclientscript/Chart.min.js"></script>
<script src="${pageContext.request.contextPath}/js/ubnclientscript/jquery.nicescroll.js"></script>
<script src="${pageContext.request.contextPath}/js/ubnclientscript/scripts.js"></script>
<script src="${pageContext.request.contextPath}/js/ubnclientscript/chartinator.js" ></script>
<script src="${pageContext.request.contextPath}/js/ubnclientscript/skycons.js"></script>
<script src="${pageContext.request.contextPath}/js/ubnclientscript/bootstrap.js"> </script>
<script src="${pageContext.request.contextPath}/js/ubnclientscript/sweetalert2.js"></script>
<script src="${pageContext.request.contextPath}/js/ubnclientscript/jquery.dataTables.min.js"> </script>
<script src="${pageContext.request.contextPath}/js/ubnclientscript/dataTables.bootstrap4.min.js"> </script>
<!-- mother grid end here-->
<script src="${pageContext.request.contextPath}/js/ubnclientcontroller/validator.js"> </script>
<script src="${pageContext.request.contextPath}/js/ubnclientcontroller/refcodevalidation.js"> </script>
<script src="${pageContext.request.contextPath}/js/ubnclientcontroller/onload.js"> </script>
<script src="${pageContext.request.contextPath}/js/ubnclientcontroller/refercustomer.js"> </script>
<script src="${pageContext.request.contextPath}/js/ubnclientcontroller/redeemcustomerpoint.js"> </script>
 <script src="${pageContext.request.contextPath}/js/ubnclientcontroller/timeoutscript.js"> </script> 
<!-- mother grid end here-->
</body>
</html>                     
