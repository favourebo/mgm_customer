<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>UBN MGM</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
      <link href="${pageContext.request.contextPath}/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="${pageContext.request.contextPath}/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="${pageContext.request.contextPath}/css/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/css/creative.css" rel="stylesheet">
	

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="images/logo2.png" height="60px"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
		    <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="${pageContext.request.contextPath}/">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">About</a>
            </li>
             <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">How It Works</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
			 <li class="nav-item">
               <a  class="nav-link js-scroll-trigger"
			    href="${pageContext.request.contextPath}/user-auth" 
			  >Login/Register</a>
             </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>UBN MGM Platform</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">Refer your friends or family to Union Bank and get rewarded!</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">ABOUT THE PLATFORM</h2>
            <hr class="light my-4">
            <p class="text-white mb-4">Member-Get-Member is a referral program from Union Bank that rewards you with points when you introduce anyone who is not an existing customer to the bank..... <a href="#" style="color:whitesmoke;text-decoration:underline">read more</a></p>
           
            <a class="btn btn-light btn-xl js-scroll-trigger" href="#services" style="color:#00AEEF">Get Started</a>
          </div>
        </div>
      </div>
    </section>

     
	 
	<section id="services">
       <div class="container">
	   <div class="col-lg-10 mx-auto">
            <h2 class="section-heading text-center" style="color:#00AEEF">How It Works</h2>
            <hr class="my-4">
            <!-- p class="text-faded mb-4" style="color:#00AEEF">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut</p-->
  <div class="row">
  
  <div class="col-lg-6" >
   <img src="${pageContext.request.contextPath}/images/how_it_works2.jpg" width="100%" />
  </div>
  
  <div class="col-lg-6" style="color:#00AEEF; font-size:15px;text-shadow: 1px 1px 1px #e2e2e2;">
     <ol>
     <li  style="padding:3px">Speak to your friends or family about Union Bank.</li>
   
     <li style="padding:3px">Access the referral form here or through any of the channels below:</li>
     <li style="padding:3px">Complete the information on the referral form</li>
     <li style="padding:3px">You will receive a notification after you have successfully completed and submitted the form</li>
     <li style="padding:3px">A customer care representative will contact the person you have referred and once the account has been successfully opened, you will be rewarded with points. </li>
     <li style="padding:3px">You can redeem your points to get gifts and lifestyle benefits.</li>
     </ol>
    </div> 
     </div>
       </div>
       



	   <!--div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">How It Works</h2>
            <hr class="my-4">
			
			<div class="col-lg-6" style="color:black">
			<p class="text-faded mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut</p>
          </div>

		  <div class="col-lg-6" style="color:black">
			<p class="text-faded mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut</p>
          </div>

		 </div>
        </div-->
      </div>
   </section>

   

    <!--section class="bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4">Free Download at Start Bootstrap!</h2>
        <a class="btn btn-light btn-xl sr-button" href="http://startbootstrap.com/template-overviews/creative/">Download Now!</a>
      </div>
    </section-->

    <section id="contact" class="bg-primary" style="color:#fff">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Let's Get In Touch!</h2>
            <hr class="my-4">
            <p class="mb-5">Give us a call or send us an email and we will get back to you as soon as possible!</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 ml-auto text-center" style="font-size:13px">
            <i class="fas fa-phone fa-3x mb-3 sr-contact-1"></i>
             <p>UnionCare: +234-1-2716816 and 07007007000 <br> WhatsApp: 09070070001</p>
            
          </div>
          <div class="col-lg-4 mr-auto text-center" style="font-size:13px">
            <i class="fas fa-envelope fa-3x mb-3 sr-contact-2"></i>
            <p>
              <a href="mailto:your-email@your-domain.com" style="color:white"> customerservice@unionbankng.com</a>
            </p>
          </div>
        </div>
      </div>
    </section>

	<!-- Bootstrap core JavaScript -->
    <script src="${pageContext.request.contextPath}/js/ubnclientscript/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/ubnclientscript/bootstrap.bundle.min.js"></script>
	 <script src="${pageContext.request.contextPath}/js/ubnclientscript/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="${pageContext.request.contextPath}/js/ubnclientscript/jquery.easing.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/ubnclientscript/scrollreveal.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/ubnclientscript/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="${pageContext.request.contextPath}/js/ubnclientscript/creative.min.js"></script>
	<script>
$(document).ready(function(){
	$(document).on('click','.signup-tab',function(e){
		e.preventDefault();
	    $('#signup-taba').tab('show');
	});	
	
	$(document).on('click','.signin-tab',function(e){
	   	e.preventDefault();
	    $('#signin-taba').tab('show');
	});
	    	
	$(document).on('click','.forgetpass-tab',function(e){
	 	e.preventDefault();
	   	$('#forgetpass-taba').tab('show');
	});
});	
</script>

  </body>

</html>
